package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.NSCStagingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.NSCStagingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;


/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class NSCStagingDAO implements NSCStagingDAOInterface<NSCStagingInterface> {

    /**
     * Requesting spring to get singleton  NSCStagingRepository object.
     */
    @Autowired
    NSCStagingRepository nscStagingRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(NSCStagingDAO.class);

    @Override
    public NSCStagingInterface get(NSCStagingInterface nscStaging) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        NSCStagingInterface existingNSCStaging = null;
        if(nscStaging != null){
            existingNSCStaging = nscStagingRepository.findOne(nscStaging.getId());
        }
        return existingNSCStaging;
    }

    @Override
    public NSCStagingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        NSCStagingInterface nscStaging = null;
        nscStaging = nscStagingRepository.findOne(id);
        return nscStaging;
    }

    @Override
    public NSCStagingInterface add(NSCStagingInterface nscStaging) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        NSCStagingInterface insertedNSCStaging = null;
        if(nscStaging != null){
            insertedNSCStaging = nscStagingRepository.save(nscStaging);
        }
        return insertedNSCStaging;
    }

    @Override
    public NSCStagingInterface update(NSCStagingInterface nscStaging) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        NSCStagingInterface updatedNSCStaging = null;
        if(nscStaging != null){
            updatedNSCStaging = nscStagingRepository.save(nscStaging);
        }
        return updatedNSCStaging;
    }

    @Override
    public boolean existsByLocationCodeAndConnectionDateBetween(String locationCode, Date fromDate, Date toDate) {
        final String methodName = "existsByLocationCodeAndConnectionDateBetween() : ";
        this.logger.info(methodName + "called");
        boolean exists = true;
        if (!StringUtils.isEmpty(locationCode) && fromDate != null && toDate != null) {
            exists = nscStagingRepository.existsByLocationCodeAndConnectionDateBetween(locationCode, fromDate, toDate);
        }
        return exists;
    }
}
