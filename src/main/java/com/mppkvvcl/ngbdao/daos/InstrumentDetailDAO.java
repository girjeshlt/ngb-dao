package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.InstrumentDetailDAOInterface;
import com.mppkvvcl.ngbdao.repositories.InstrumentDetailRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class InstrumentDetailDAO implements InstrumentDetailDAOInterface<InstrumentDetailInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private static final Logger logger = GlobalResources.getLogger(InstrumentDetailDAO.class);

    @Autowired
    private InstrumentDetailRepository instrumentDetailRepository;
    
    @Override
    public InstrumentDetailInterface add(InstrumentDetailInterface instrumentDetail) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface insertedInstrumentDetail = null;
        if (instrumentDetail != null){
            insertedInstrumentDetail = instrumentDetailRepository.save(instrumentDetail);
        }
        return insertedInstrumentDetail;
    }

    @Override
    public InstrumentDetailInterface update(InstrumentDetailInterface instrumentDetail) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface updatedInstrumentDetail = null;
        if (instrumentDetail != null){
            updatedInstrumentDetail = instrumentDetailRepository.save(instrumentDetail);
        }
        return updatedInstrumentDetail;
    }

    @Override
    public InstrumentDetailInterface get(InstrumentDetailInterface instrumentDetail) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface existingInstrumentDetail = null;
        if (instrumentDetail != null){
            existingInstrumentDetail = instrumentDetailRepository.findOne(instrumentDetail.getId());
        }
        return existingInstrumentDetail;
    }

    @Override
    public InstrumentDetailInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface existingInstrumentDetail = null;
        if(id != 0){
            existingInstrumentDetail = instrumentDetailRepository.findOne(id);
        }
        return existingInstrumentDetail;
    }

    @Override
    public void deleteById(long id) {
        final String methodName = "deleteById() : ";
        logger.info(methodName + "called");
        if(id != 0){
             instrumentDetailRepository.delete(id);
        }
    }

    @Override
    public List<InstrumentDetailInterface> getByDishonoured(boolean dishonoured) {
        final String methodName = "getByDishonoured() : ";
        logger.info(methodName + "called");
        List<InstrumentDetailInterface> existingInstrumentDetail = null;
        existingInstrumentDetail = instrumentDetailRepository.findByDishonoured(dishonoured);
        return existingInstrumentDetail;
    }

    @Override
    public InstrumentDetailInterface getByPayModeAndBankNameAndInstrumentNo(String payMode, String bankName, String instrumentNo) {
        final String methodName = "getByPayModeAndBankNameAndInstrumentNo() : ";
        logger.info(methodName + "called");
        InstrumentDetailInterface existingInstrumentDetail = null;
        if(payMode != null && bankName != null && instrumentNo != null){
            existingInstrumentDetail = instrumentDetailRepository.findByPayModeAndBankNameAndInstrumentNo(payMode,bankName,instrumentNo);
        }
        return existingInstrumentDetail;
    }

    @Override
    public List<InstrumentDetailInterface> getByInstrumentNo(final String instrumentNo) {
        final String methodName = "getByInstrumentNo() : ";
        logger.info(methodName + "called");
        List<InstrumentDetailInterface> instrumentDetails = null;
        if (instrumentNo != null && !instrumentNo.isEmpty()) {
            instrumentDetails = instrumentDetailRepository.findByInstrumentNo(instrumentNo);
        }
        return instrumentDetails;
    }
}
