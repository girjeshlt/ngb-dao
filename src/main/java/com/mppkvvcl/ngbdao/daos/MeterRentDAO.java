package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterRentDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterRentRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterRentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterRentDAO implements MeterRentDAOInterface<MeterRentInterface> {

    Logger logger = GlobalResources.getLogger(MeterRentDAO.class);

    @Autowired
    MeterRentRepository meterRentRepository ;

    @Override
    public MeterRentInterface add(MeterRentInterface meterRent) {
        final String methodName = "add()  : ";
        logger.info(methodName + "clicked");
        MeterRentInterface insertedMeterRent = null;
        if (meterRent != null){
            insertedMeterRent = meterRentRepository.save(meterRent);
        }
        return insertedMeterRent;
    }

    @Override
    public MeterRentInterface update(MeterRentInterface meterRent) {
        final String methodName = "update()  : ";
        logger.info(methodName + "clicked");
        MeterRentInterface updateMeterRent = null;
        if (meterRent != null){
            updateMeterRent = meterRentRepository.save(meterRent);
        }
        return updateMeterRent;
    }

    @Override
    public MeterRentInterface get(MeterRentInterface meterRent) {
        final String methodName = "get()  : ";
        logger.info(methodName + "clicked");
        MeterRentInterface getMeterRent = null;
        if (meterRent != null){
            getMeterRent = meterRentRepository.findOne(meterRent.getId());
        }
        return getMeterRent;
    }

    @Override
    public MeterRentInterface getById(long id) {
        final String methodName = "getById()  : ";
        logger.info(methodName + "clicked");
        MeterRentInterface meterRent = null;
        if (id > 0){
            meterRent = meterRentRepository.findOne(id);
        }
        return meterRent;
    }

    @Override
    public MeterRentInterface getByMeterCodeAndMeterCapacity(String meterCode, String meterCapacity) {
        final String methodName  = "getByMeterCodeAndMeterCapacity() : ";
        logger.info(methodName + "clicked");
        MeterRentInterface meterRent =  null;
        if (meterCode != null && meterCapacity != null){
            meterRent = meterRentRepository.findByMeterCodeAndMeterCapacity(meterCode , meterCapacity);
        }
        return meterRent;
    }

    @Override
    public List<MeterRentInterface> getByMeterCode(String meterCode) {
        final String methodName = "getByMeterCode() : ";
        logger.info(methodName + "clicked");
        List<MeterRentInterface> meterRents = null;
        if (meterCode != null){
            meterRents = meterRentRepository.findByMeterCode(meterCode);
        }
        return meterRents;
    }

    @Override
    public List<MeterRentInterface> getByMeterRent(BigDecimal meterRent) {
        final String methodName = "getByMeterRent() : ";
        logger.info(methodName + "clicked");
        List<MeterRentInterface> meterRents = null;
        if (meterRent != null){
            meterRents = meterRentRepository.findByMeterRent(meterRent);
        }
        return meterRents;
    }

    @Override
    public MeterRentInterface getByMeterCodeAndEffectiveStartDate(String meterCode, Date effectiveDate) {
        final String methodName = "getByMeterCodeAndEffectiveDate() : ";
        logger.info(methodName + "clicked");
        MeterRentInterface meterRent = null;
        if (meterCode != null && effectiveDate != null){
            meterRent  = meterRentRepository.findByMeterCodeAndEffectiveStartDate(meterCode , effectiveDate);
        }
        return meterRent;
    }

    @Override
    public MeterRentInterface getByMeterRentAndEffectiveStartDate(BigDecimal meterRent, Date effectiveDate) {
        final String methodName = "getByMeterRentAndEffectiveDate() : ";
        logger.info(methodName + "clicked");
        MeterRentInterface effectiveMeterRent =  null;
        if (meterRent != null && effectiveDate != null){
            effectiveMeterRent = meterRentRepository.findByMeterRentAndEffectiveStartDate(meterRent , effectiveDate);
        }
        return effectiveMeterRent;
    }
}
