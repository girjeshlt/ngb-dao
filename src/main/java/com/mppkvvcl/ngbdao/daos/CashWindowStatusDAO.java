package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.CashWindowStatusDAOInterface;
import com.mppkvvcl.ngbdao.repositories.CashWindowStatusRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/15/2017.
 */
@Service
public class CashWindowStatusDAO implements CashWindowStatusDAOInterface<CashWindowStatusInterface> {
    @Autowired
    private CashWindowStatusRepository cashWindowStatusRepository;
    /**
     * Get logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(CashWindowStatusDAO.class);

    @Override
    public List<CashWindowStatusInterface> getByUsernameAndStatus(String username, String status) {
        final String methodName = "getByUsernameAndStatus() : ";
        logger.info(methodName + "is called");
        List <CashWindowStatusInterface> cashWindowStatus = null;
        if(username  != null && status != null){
            cashWindowStatus = cashWindowStatusRepository.findByUsernameAndStatus(username,status);
        }
        return cashWindowStatus;
    }

    @Override
    public List<CashWindowStatusInterface> getByLocationCodeAndDateAndStatus(String location, Date date, String status) {
        final String methodName = "getByLocationCodeAndDateAndStatus() : ";
        logger.info(methodName + "is called");
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        if(location != null && date != null && status != null){
            cashWindowStatuses = cashWindowStatusRepository.findByLocationCodeAndDateAndStatus(location, date, status);
        }
        return cashWindowStatuses;
    }

    @Override
    public CashWindowStatusInterface getByLocationCodeAndWindowNameAndDate(String locationCode, String windowName, Date freezeOnDate) {
        final String methodName = "getByLocationCodeAndWindowNameAndDate() : ";
        logger.info(methodName + "is called");
        CashWindowStatusInterface cashWindowStatus = null;
        if(locationCode != null && windowName != null && freezeOnDate != null){
            cashWindowStatus = cashWindowStatusRepository.findByLocationCodeAndWindowNameAndDate(locationCode, windowName, freezeOnDate);
        }
        return cashWindowStatus;
    }

    @Override
    public List<CashWindowStatusInterface> getByLocationCodeAndStatus(String locationCode, String statusOpen) {
        final String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "is called");
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        if (locationCode != null && statusOpen != null){
            cashWindowStatuses = cashWindowStatusRepository.findByLocationCodeAndStatus(locationCode, statusOpen);
        }

        return cashWindowStatuses;
    }

    @Override
    public List<CashWindowStatusInterface> getByUsernameAndStatusAndDate(String username, String status, Date freezeOnDate) {
        final String methodName = "getByUsernameAndStatusAndDate() : ";
        logger.info(methodName + " called");
        List<CashWindowStatusInterface> cashWindowStatuses = null;
        if(username != null && status != null && freezeOnDate != null){
            cashWindowStatuses = cashWindowStatusRepository.findByUsernameAndStatusAndDate(username, status, freezeOnDate);
        }
        return cashWindowStatuses;
    }

    @Override
    public CashWindowStatusInterface get(CashWindowStatusInterface cashWindowStatus) {
        final String methodName = "get() : ";
        logger.info(methodName + " called");
        CashWindowStatusInterface existingCashWindowStatus = null;
        if(cashWindowStatus != null){
            existingCashWindowStatus = cashWindowStatusRepository.findOne(cashWindowStatus.getId());
        }
        return existingCashWindowStatus;
    }

    @Override
    public CashWindowStatusInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        CashWindowStatusInterface cashWindowStatus = null;
        cashWindowStatus = cashWindowStatusRepository.findOne(id);
        return cashWindowStatus;
    }

    @Override
    public CashWindowStatusInterface add(CashWindowStatusInterface cashWindowStatus) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        CashWindowStatusInterface insertedCashWindowStatus = null;
        if(cashWindowStatus != null){
            insertedCashWindowStatus = cashWindowStatusRepository.save(cashWindowStatus);
        }
        return insertedCashWindowStatus;
    }

    @Override
    public CashWindowStatusInterface update(CashWindowStatusInterface cashWindowStatus) {
        final String methodName = "update() :";
        logger.info(methodName + "called");
        CashWindowStatusInterface updatedCashWindowStatus = null;
        if(cashWindowStatus != null){
            updatedCashWindowStatus = cashWindowStatusRepository.save(cashWindowStatus);
        }
        return updatedCashWindowStatus;
    }
}
