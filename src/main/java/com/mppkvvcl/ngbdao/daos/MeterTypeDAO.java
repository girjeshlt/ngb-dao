package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterTypeDAO implements MeterTypeDAOInterface<MeterTypeInterface> {

    Logger logger = GlobalResources.getLogger(MeterTypeDAO.class);

    @Autowired
    MeterTypeRepository meterTypeRepository;

    @Override
    public MeterTypeInterface add(MeterTypeInterface meterType) {
        final String methodName = "add() :  ";
        logger.info(methodName+"clicked");
        MeterTypeInterface insertedMeterType= null;
        if (meterType != null){
            insertedMeterType = meterTypeRepository.save(meterType);
        }
        return insertedMeterType;
    }

    @Override
    public MeterTypeInterface update(MeterTypeInterface meterType) {
        final String methodName = "update() :  ";
        logger.info(methodName+"clicked");
        MeterTypeInterface updatedMeterType= null;
        if (meterType != null){
            updatedMeterType = meterTypeRepository.save(meterType);
        }
        return updatedMeterType;
    }

    @Override
    public MeterTypeInterface get(MeterTypeInterface meterType) {
        final String methodName = "get() :  ";
        logger.info(methodName+"clicked");
        MeterTypeInterface getMeterType= null;
        if (meterType != null){
            getMeterType = meterTypeRepository.findOne(meterType.getId());
        }
        return getMeterType;
    }

    @Override
    public MeterTypeInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName+"clicked");
        MeterTypeInterface meterType= null;
        if (id > 0){
            meterType = meterTypeRepository.findOne(id);
        }
        return meterType;
    }

    @Override
    public List<MeterTypeInterface> getByMeterPhase(String meterPhase) {
        final String methodName = "getByMeterPhase() : ";
        logger.info(methodName+"clicked");
        List<MeterTypeInterface> meterTypes = null;
        if (meterPhase != null){
            meterTypes = meterTypeRepository.findByMeterPhase(meterPhase);
        }
        return meterTypes;
    }

    @Override
    public List<? extends MeterTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return meterTypeRepository.findAll();
    }
}
