package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.XrayConnectionFixedChargeRateDAOInterface;
import com.mppkvvcl.ngbdao.repositories.XrayConnectionFixedChargeRateRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionFixedChargeRateInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 12/18/2017.
 */
@Service
public class XrayConnectionFixedChargeRateDAO implements XrayConnectionFixedChargeRateDAOInterface<XrayConnectionFixedChargeRateInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(XrayConnectionFixedChargeRateDAO.class);

    @Autowired
    private XrayConnectionFixedChargeRateRepository xrayConnectionFixedChargeRateRepository;

    @Override
    public XrayConnectionFixedChargeRateInterface add(XrayConnectionFixedChargeRateInterface xrayConnectionFixedChargeRateInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        XrayConnectionFixedChargeRateInterface insertedXrayConnectionFixedChargeRateInterface = null;
        if (xrayConnectionFixedChargeRateInterface != null){
            insertedXrayConnectionFixedChargeRateInterface = xrayConnectionFixedChargeRateRepository.save(xrayConnectionFixedChargeRateInterface);
        }
        return insertedXrayConnectionFixedChargeRateInterface;
    }

    @Override
    public XrayConnectionFixedChargeRateInterface update(XrayConnectionFixedChargeRateInterface xrayConnectionFixedChargeRateInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        XrayConnectionFixedChargeRateInterface updatedXrayConnectionFixedChargeRateInterface = null;
        if (xrayConnectionFixedChargeRateInterface != null){
            updatedXrayConnectionFixedChargeRateInterface = xrayConnectionFixedChargeRateRepository.save(xrayConnectionFixedChargeRateInterface);
        }
        return updatedXrayConnectionFixedChargeRateInterface;
    }

    @Override
    public XrayConnectionFixedChargeRateInterface get(XrayConnectionFixedChargeRateInterface xrayConnectionFixedChargeRateInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        XrayConnectionFixedChargeRateInterface existingXrayConnectionFixedChargeRateInterface = null;
        if (xrayConnectionFixedChargeRateInterface != null){
            existingXrayConnectionFixedChargeRateInterface = xrayConnectionFixedChargeRateRepository.findOne(xrayConnectionFixedChargeRateInterface.getId());
        }
        return existingXrayConnectionFixedChargeRateInterface;
    }

    @Override
    public XrayConnectionFixedChargeRateInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        XrayConnectionFixedChargeRateInterface existingXrayConnectionFixedChargeRateInterface = null;
        if(id != 0){
            existingXrayConnectionFixedChargeRateInterface = xrayConnectionFixedChargeRateRepository.findOne(id);
        }
        return existingXrayConnectionFixedChargeRateInterface;    }

    @Override
    public List<XrayConnectionFixedChargeRateInterface> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        List<XrayConnectionFixedChargeRateInterface> fetchedXrayConnectionFixedChargeRateInterfaces = xrayConnectionFixedChargeRateRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return fetchedXrayConnectionFixedChargeRateInterfaces;
    }

    @Override
    public XrayConnectionFixedChargeRateInterface getByTariffIdAndSubcategoryCodeAndXrayType(long tariffId, long subcategoryCode, String xrayType) {
        final String methodName = "getByTariffIdAndSubcategoryCodeAndXrayType() : ";
        logger.info(methodName + "called");
        XrayConnectionFixedChargeRateInterface fetchedXrayConnectionFixedChargeRateInterface = null;
        if(xrayType != null) {
            fetchedXrayConnectionFixedChargeRateInterface = xrayConnectionFixedChargeRateRepository.findByTariffIdAndSubcategoryCodeAndXrayType(tariffId,subcategoryCode,xrayType);
        }
        return fetchedXrayConnectionFixedChargeRateInterface;
    }

}
