package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentHierarchyInterface;
import com.mppkvvcl.ngbdao.interfaces.AdjustmentHierarchyDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdjustmentHierarchyRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 15-09-2017.
 */
@Service
public class AdjustmentHierarchyDAO implements AdjustmentHierarchyDAOInterface<AdjustmentHierarchyInterface> {

    /**
     * Requesting spring to get singleton AdjustmentHierarchyRepository object.
     */
    @Autowired
    private AdjustmentHierarchyRepository adjustmentHierarchyRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentHierarchyDAO.class);

    @Override
    public List<AdjustmentHierarchyInterface> getByPriorityBetweenUserPriorityAndAdjustmentPriority(int userPriority, int adjustmentPriority) {
        final String methodName = "getByPriorityBetweenUserPriorityAndAdjustmentPriority() : ";
        logger.info(methodName + "is called");
        List<AdjustmentHierarchyInterface> adjustmentHierarchies = null;
        adjustmentHierarchies = adjustmentHierarchyRepository.findByPriorityBetweenUserPriorityAndAdjustmentPriority(userPriority,adjustmentPriority);
        return adjustmentHierarchies;
    }

    @Override
    public AdjustmentHierarchyInterface get(AdjustmentHierarchyInterface adjustmentHierarchy) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AdjustmentHierarchyInterface existingAdjustmentHierarchy = null;
        if(adjustmentHierarchy != null){
            existingAdjustmentHierarchy = adjustmentHierarchyRepository.findOne(adjustmentHierarchy.getId());
        }
        return existingAdjustmentHierarchy;
    }

    @Override
    public AdjustmentHierarchyInterface add(AdjustmentHierarchyInterface adjustmentHierarchy) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AdjustmentHierarchyInterface insertedAdjustmentHierarchy = null;
        if(adjustmentHierarchy != null){
            insertedAdjustmentHierarchy = adjustmentHierarchyRepository.save(adjustmentHierarchy);
        }
        return insertedAdjustmentHierarchy;
    }

    @Override
    public AdjustmentHierarchyInterface update(AdjustmentHierarchyInterface adjustmentHierarchy) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AdjustmentHierarchyInterface updatedAdjustmentHierarchy = null;
        if(adjustmentHierarchy != null){
            updatedAdjustmentHierarchy = adjustmentHierarchyRepository.save(adjustmentHierarchy);
        }
        return updatedAdjustmentHierarchy;
    }

    @Override
    public AdjustmentHierarchyInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AdjustmentHierarchyInterface adjustmentHierarchy = null;
        adjustmentHierarchy = adjustmentHierarchyRepository.findOne(id);
        return adjustmentHierarchy;
    }
}
