package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerInformationMetaDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerInformationMetaRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationMetaInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SHIVANSHU on 15-09-2017.
 */
@Service
public class ConsumerInformationMetaDAO implements ConsumerInformationMetaDAOInterface<ConsumerInformationMetaInterface> {

    Logger logger = GlobalResources.getLogger(ConsumerInformationMetaDAO.class);

    @Autowired
    ConsumerInformationMetaRepository consumerInformationMetaRepository;

    @Override
    public ConsumerInformationMetaInterface add(ConsumerInformationMetaInterface consumerInformationMeta) {
        final String methodName = "add() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationMetaInterface insertedConsumerInformationMeta = null;
        if (consumerInformationMeta!= null){
            insertedConsumerInformationMeta = consumerInformationMetaRepository.save(consumerInformationMeta);
        }
        return insertedConsumerInformationMeta;
    }

    @Override
    public ConsumerInformationMetaInterface update(ConsumerInformationMetaInterface consumerInformationMeta) {
        final String methodName = "update() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationMetaInterface newConsumerInformationMeta = null;
        if (consumerInformationMeta!= null){
            newConsumerInformationMeta = consumerInformationMetaRepository.save(consumerInformationMeta);
        }
        return newConsumerInformationMeta;
    }

    @Override
    public ConsumerInformationMetaInterface get(ConsumerInformationMetaInterface consumerInformationMeta) {
        final String methodName = "get() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationMetaInterface oldConsumerInformationMeta = null;
        if (consumerInformationMeta!= null){
            oldConsumerInformationMeta = consumerInformationMetaRepository.findOne(consumerInformationMeta.getId());
        }
        return oldConsumerInformationMeta;
    }

    @Override
    public ConsumerInformationMetaInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationMetaInterface oldConsumerInformationMeta = null;
        if (id != 0){
            oldConsumerInformationMeta = consumerInformationMetaRepository.findOne(id);
        }
        return oldConsumerInformationMeta;
    }
}
