package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionPaymentInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionPaymentInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionPaymentInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionPaymentInformationDAO implements ConsumerConnectionPaymentInformationDAOInterface<ConsumerConnectionPaymentInformationInterface> {
    @Autowired
    ConsumerConnectionPaymentInformationRepository consumerConnectionPaymentInformationRepository;

    Logger logger = GlobalResources.getLogger(ConsumerConnectionPaymentInformationDAO.class);

    @Override
    public ConsumerConnectionPaymentInformationInterface add(ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerConnectionPaymentInformationInterface insertedConsumerConnectionPaymentInformation = null;
        if(consumerConnectionPaymentInformation != null){
            insertedConsumerConnectionPaymentInformation = consumerConnectionPaymentInformationRepository.save(consumerConnectionPaymentInformation);
        }
        return insertedConsumerConnectionPaymentInformation;
    }

    @Override
    public ConsumerConnectionPaymentInformationInterface get(ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionPaymentInformationInterface existingConsumerConnectionPaymentInformation = null;
        if(consumerConnectionPaymentInformation != null){
            existingConsumerConnectionPaymentInformation = consumerConnectionPaymentInformationRepository.findOne(consumerConnectionPaymentInformation.getId());
        }
        return existingConsumerConnectionPaymentInformation;
    }

    @Override
    public ConsumerConnectionPaymentInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation = null;
        consumerConnectionPaymentInformation = consumerConnectionPaymentInformationRepository.findOne(id);
        return consumerConnectionPaymentInformation;
    }

    @Override
    public ConsumerConnectionPaymentInformationInterface update(ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionPaymentInformationInterface updatedConsumerConnectionPaymentInformation = null;
        if(consumerConnectionPaymentInformation != null){
            updatedConsumerConnectionPaymentInformation = consumerConnectionPaymentInformationRepository.save(consumerConnectionPaymentInformation);
        }
        return updatedConsumerConnectionPaymentInformation;
    }
}
