package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionInformationMetaDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionInformationMetaRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationMetaInterface;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionInformationMetaDAO implements ConsumerConnectionInformationMetaDAOInterface<ConsumerConnectionInformationMetaInterface> {

    @Autowired
    private ConsumerConnectionInformationMetaRepository consumerConnectionInformationMetaRepository;
    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationMetaDAO.class);

    @Override
    public ConsumerConnectionInformationMetaInterface add(ConsumerConnectionInformationMetaInterface consumerConnectionInformationMeta) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationMetaInterface insertedConsumerConnectionInformationMeta = null;
        if(consumerConnectionInformationMeta != null){
            insertedConsumerConnectionInformationMeta = consumerConnectionInformationMetaRepository.save(consumerConnectionInformationMeta);
        }
        return insertedConsumerConnectionInformationMeta;
    }

    @Override
    public ConsumerConnectionInformationMetaInterface get(ConsumerConnectionInformationMetaInterface consumerConnectionInformationMeta) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationMetaInterface existingConsumerConnectionInformationMeta = null;
        existingConsumerConnectionInformationMeta = consumerConnectionInformationMetaRepository.findOne(consumerConnectionInformationMeta.getId());
        return existingConsumerConnectionInformationMeta;
    }

    @Override
    public ConsumerConnectionInformationMetaInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationMetaInterface consumerConnectionInformationMeta = null;
        consumerConnectionInformationMeta = consumerConnectionInformationMetaRepository.findOne(id);
        return consumerConnectionInformationMeta;
    }

    @Override
    public ConsumerConnectionInformationMetaInterface update(ConsumerConnectionInformationMetaInterface consumerConnectionInformationMeta) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationMetaInterface updatedConsumerConnectionInformationMeta = null;
        if(consumerConnectionInformationMeta != null){
            updatedConsumerConnectionInformationMeta = consumerConnectionInformationMetaRepository.save(consumerConnectionInformationMeta);
        }
        return updatedConsumerConnectionInformationMeta;
    }
}
