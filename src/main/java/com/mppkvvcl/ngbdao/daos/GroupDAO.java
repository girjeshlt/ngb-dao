package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.GroupDAOInterface;
import com.mppkvvcl.ngbdao.repositories.GroupRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 10/3/2017.
 */
@Service
public class GroupDAO implements GroupDAOInterface<GroupInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(GroupDAO.class);

    /**
     * Requesting spring to get singleton  GroupRepository object.
     */
    @Autowired
    private GroupRepository groupRepository;

    @Override
    public GroupInterface get(GroupInterface group) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        GroupInterface existingGroup = null;
        if(group != null){
            existingGroup = groupRepository.findOne(group.getId());
        }
        return existingGroup;
    }

    @Override
    public GroupInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        GroupInterface group = null;
        group = groupRepository.findOne(id);
        return group;
    }

    @Override
    public GroupInterface add(GroupInterface group) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        GroupInterface insertedGroup = null;
        if(group != null){
            insertedGroup = groupRepository.save(group);
        }
        return insertedGroup;
    }

    @Override
    public GroupInterface update(GroupInterface group) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        GroupInterface updatedGroup = null;
        if(group != null){
            updatedGroup = groupRepository.save(group);
        }
        return updatedGroup;
    }

    @Override
    public List<GroupInterface> getByLocationCode(String locationCode) {
        final String methodName = "getByLocationCode() :  ";
        logger.info(methodName + "called");
        List<GroupInterface> groups =  null;
        if (locationCode != null){
            groups = groupRepository.findByLocationCode(locationCode);
        }
        return groups;
    }

    @Override
    public GroupInterface getByGroupNo(String groupNo) {
        final String methodName = "getByGroupNo() :  ";
        logger.info(methodName+"called");
        GroupInterface groupInterface = null;
        if(groupNo != null){
            groupInterface = groupRepository.findByGroupNo(groupNo);
        }
        return groupInterface;
    }
}
