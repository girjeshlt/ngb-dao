package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;
import com.mppkvvcl.ngbdao.interfaces.AdjustmentTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdjustmentTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class AdjustmentTypeDAO implements AdjustmentTypeDAOInterface<AdjustmentTypeInterface> {

    /**
     * Requesting spring to get singleton AdjustmentTypeRepository object.
     */
    @Autowired
    private AdjustmentTypeRepository adjustmentTypeRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(AdjustmentTypeDAO.class);

    @Override
    public AdjustmentTypeInterface getByCode(int code) {
        final String methodName = "getByCode() : ";
        logger.info(methodName + "is called");
        AdjustmentTypeInterface adjustmentType = null;
        if(code > 0){
            adjustmentType = adjustmentTypeRepository.findByCode(code);
        }
        return adjustmentType;
    }

    @Override
    public List<AdjustmentTypeInterface> getAllByView(boolean view) {
        final String methodName = "getAllByView() : ";
        logger.info(methodName + "is called");
        List<AdjustmentTypeInterface> adjustmentTypes = null;
        adjustmentTypes = adjustmentTypeRepository.findAllByView(view);
        return adjustmentTypes;
    }

    @Override
    public AdjustmentTypeInterface add(AdjustmentTypeInterface adjustmentType) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AdjustmentTypeInterface insertedAdjustmentType = null;
        if(adjustmentType != null){
            insertedAdjustmentType = adjustmentTypeRepository.save(adjustmentType);
        }
        return insertedAdjustmentType;
    }

    @Override
    public AdjustmentTypeInterface update(AdjustmentTypeInterface adjustmentType) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AdjustmentTypeInterface updatedAdjustmentType = null;
        if(adjustmentType != null){
            updatedAdjustmentType = adjustmentTypeRepository.save(adjustmentType);
        }
        return updatedAdjustmentType;
    }

    @Override
    public AdjustmentTypeInterface get(AdjustmentTypeInterface adjustmentType) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AdjustmentTypeInterface existingAdjustmentType = null;
        if(adjustmentType != null){
            existingAdjustmentType = adjustmentTypeRepository.findOne(adjustmentType.getId());
        }
        return existingAdjustmentType;
    }

    @Override
    public AdjustmentTypeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AdjustmentTypeInterface adjustmentType = null;
        adjustmentType = adjustmentTypeRepository.findOne(id);
        return adjustmentType;
    }

    public List<? extends AdjustmentTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "is called");
        List<? extends AdjustmentTypeInterface> adjustmentTypes = null;
        adjustmentTypes = adjustmentTypeRepository.findAll();
        return adjustmentTypes;
    }
}
