package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SurchargeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SurchargeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SurchargeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class SurchargeDAO implements SurchargeDAOInterface<SurchargeInterface> {


    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(SurchargeDAO.class);

    @Autowired
    private SurchargeRepository surchargeRepository;

    @Override
    public SurchargeInterface add(SurchargeInterface surcharge) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SurchargeInterface insertedSurcharge = null;
        if (surcharge != null){
            insertedSurcharge = surchargeRepository.save(surcharge);
        }
        return insertedSurcharge;
    }

    @Override
    public SurchargeInterface update(SurchargeInterface surcharge) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SurchargeInterface updatedSurcharge = null;
        if (surcharge != null){
            updatedSurcharge = surchargeRepository.save(surcharge);
        }
        return updatedSurcharge;
    }

    @Override
    public SurchargeInterface get(SurchargeInterface surcharge) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SurchargeInterface existingSurcharge = null;
        if (surcharge != null){
            existingSurcharge = surchargeRepository.findOne(surcharge.getId());
        }
        return existingSurcharge;
    }

    @Override
    public SurchargeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SurchargeInterface existingSurcharge = null;
        if(id != 0){
            existingSurcharge = surchargeRepository.findOne(id);
        }
        return existingSurcharge;
    }

    @Override
    public List<SurchargeInterface> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        List<SurchargeInterface> existingSurcharge = null;
        if(tariffId != 0 && subcategoryCode != 0){
            existingSurcharge = surchargeRepository.findByTariffIdAndSubcategoryCode(tariffId, subcategoryCode);
        }
        return existingSurcharge; 
    }

    @Override
    public List<SurchargeInterface> getByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual(long tariffId, long subcategoryCode, BigDecimal outstandingAmount) {
        final String methodName = "getByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual() : ";
        logger.info(methodName + "called");
        List<SurchargeInterface> existingSurcharge = null;
        if(tariffId != 0 && subcategoryCode != 0 && outstandingAmount != null){
            existingSurcharge = surchargeRepository.findByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual(tariffId, subcategoryCode, outstandingAmount);
        }
        return existingSurcharge;
    }
}
