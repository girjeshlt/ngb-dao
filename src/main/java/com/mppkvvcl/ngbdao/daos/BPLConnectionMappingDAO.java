package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.BPLConnectionMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BPLConnectionMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class BPLConnectionMappingDAO implements BPLConnectionMappingDAOInterface<BPLConnectionMappingInterface> {

    /**
     * Requesting spring to get singleton  BPLConnectionMappingRepository object.
     */
    @Autowired
    private BPLConnectionMappingRepository bplConnectionMappingRepository;


    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(BPLConnectionMappingDAO.class);

    @Override
    public List<BPLConnectionMappingInterface> getBetweenStartDateAndEndDate(Date givenDate) {
        final String methodName = "getBetweenStartDateAndEndDate() : ";
        logger.info(methodName + "is called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(givenDate != null){
            bplConnectionMappings = bplConnectionMappingRepository.findBetweenStartDateAndEndDate(givenDate);
        }
        return bplConnectionMappings;
    }

    @Override
    public List<BPLConnectionMappingInterface> getByBplNo(String bplNo) {
        final String methodName = "getByBplNo() : ";
        logger.info(methodName + "is called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(bplNo != null){
            bplConnectionMappings = bplConnectionMappingRepository.findByBplNo(bplNo);
        }
        return bplConnectionMappings;
    }

    @Override
    public List<BPLConnectionMappingInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "is called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(consumerNo != null){
            bplConnectionMappings = bplConnectionMappingRepository.findByConsumerNo(consumerNo);
        }
        return bplConnectionMappings;
    }

    @Override
    public List<BPLConnectionMappingInterface> getByStatus(String status) {
        final String methodName = "getByStatus() : ";
        logger.info(methodName + "is called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(status != null){
            bplConnectionMappings = bplConnectionMappingRepository.findByStatus(status);
        }
        return bplConnectionMappings;
    }

    @Override
    public BPLConnectionMappingInterface add(BPLConnectionMappingInterface bplConnectionMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        BPLConnectionMappingInterface insertedBPLConnectionMapping = null;
        if(bplConnectionMapping != null){
            insertedBPLConnectionMapping = bplConnectionMappingRepository.save(bplConnectionMapping);
        }
        return insertedBPLConnectionMapping;
    }

    @Override
    public BPLConnectionMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        BPLConnectionMappingInterface bplConnectionMapping = null;
        bplConnectionMapping = bplConnectionMappingRepository.findOne(id);
        return bplConnectionMapping;
    }

    @Override
    public BPLConnectionMappingInterface update(BPLConnectionMappingInterface bplConnectionMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        BPLConnectionMappingInterface updatedBPLConnectionMapping = null;
        if(bplConnectionMapping != null){
            updatedBPLConnectionMapping = bplConnectionMappingRepository.save(bplConnectionMapping);
        }
        return updatedBPLConnectionMapping;
    }

    @Override
    public BPLConnectionMappingInterface get(BPLConnectionMappingInterface bplConnectionMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        BPLConnectionMappingInterface existingBPLConnectionMapping = null;
        if(bplConnectionMapping != null) {
            existingBPLConnectionMapping = bplConnectionMappingRepository.findOne(bplConnectionMapping.getId());
        }
        return existingBPLConnectionMapping;
    }

    @Override
    public List<BPLConnectionMappingInterface> getByConsumerNoAndStatus(String consumerNo, String status) {
        final String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(consumerNo != null && status != null){
            bplConnectionMappings = bplConnectionMappingRepository.findByConsumerNoAndStatus(consumerNo,status);
        }
        return bplConnectionMappings;
    }

    @Override
    public List<BPLConnectionMappingInterface> getByBplNoAndStatus(String bplNo, String status) {
        final String methodName = "getByBplNoAndStatus() : ";
        logger.info(methodName + "called");
        List<BPLConnectionMappingInterface> bplConnectionMappings = null;
        if(bplNo != null && status != null){
            bplConnectionMappings = bplConnectionMappingRepository.findByBplNoAndStatus(bplNo,status);
        }
        return bplConnectionMappings;
    }
}
