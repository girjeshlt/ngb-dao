package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionMeterInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionMeterInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionMeterInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionMeterInformationDAO implements ConsumerConnectionMeterInformationDAOInterface<ConsumerConnectionMeterInformationInterface> {
    @Autowired
    private ConsumerConnectionMeterInformationRepository consumerConnectionMeterInformationRepository;

    private Logger logger = GlobalResources.getLogger(ConsumerConnectionMeterInformationDAO.class);

    @Override
    public ConsumerConnectionMeterInformationInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation = null;
        if(consumerNo != null){
            consumerConnectionMeterInformation = consumerConnectionMeterInformationRepository.findByConsumerNo(consumerNo);
        }
        return consumerConnectionMeterInformation;
    }

    @Override
    public ConsumerConnectionMeterInformationInterface add(ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerConnectionMeterInformationInterface insertedConsumerConnectionMeterInformation = null;
        if(consumerConnectionMeterInformation != null){
            insertedConsumerConnectionMeterInformation = consumerConnectionMeterInformationRepository.save(consumerConnectionMeterInformation);
        }
        return insertedConsumerConnectionMeterInformation;
    }

    @Override
    public ConsumerConnectionMeterInformationInterface get(ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionMeterInformationInterface existingConsumerConnectionMeterInformation = null;
        if(consumerConnectionMeterInformation != null){
            existingConsumerConnectionMeterInformation = consumerConnectionMeterInformationRepository.findOne(consumerConnectionMeterInformation.getId());
        }
        return existingConsumerConnectionMeterInformation;
    }

    @Override
    public ConsumerConnectionMeterInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation = null;
        consumerConnectionMeterInformation = consumerConnectionMeterInformationRepository.findOne(id);
        return consumerConnectionMeterInformation;
    }

    @Override
    public ConsumerConnectionMeterInformationInterface update(ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionMeterInformationInterface updatedConsumerConnectionMeterInformation = null;
        if(consumerConnectionMeterInformation != null){
            updatedConsumerConnectionMeterInformation = consumerConnectionMeterInformationRepository.save(consumerConnectionMeterInformation);
        }
        return updatedConsumerConnectionMeterInformation;
    }
}
