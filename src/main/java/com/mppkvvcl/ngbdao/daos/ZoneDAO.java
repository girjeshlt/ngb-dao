package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;
import com.mppkvvcl.ngbdao.interfaces.ZoneDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ZoneRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ZoneDAO implements ZoneDAOInterface<ZoneInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(ZoneDAO.class);

    @Autowired
    private ZoneRepository zoneRepository;

    @Override
    public ZoneInterface add(ZoneInterface zone) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ZoneInterface insertedZone = null;
        if(zone != null){
            insertedZone = zoneRepository.save(zone);
        }
        return insertedZone;
    }

    @Override
    public ZoneInterface get(ZoneInterface zone) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ZoneInterface existingZone = null;
        if(zone != null){
            existingZone = zoneRepository.findOne(zone.getId());
        }
        return existingZone;
    }

    @Override
    public ZoneInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ZoneInterface zone = null;
        zone = zoneRepository.findOne(id);
        return zone;
    }

    @Override
    public ZoneInterface update(ZoneInterface zone) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ZoneInterface updatedZone = null;
        if(zone != null){
            updatedZone = zoneRepository.save(zone);
        }
        return updatedZone;
    }

    @Override
    public ZoneInterface getByLocationCode(String locationCode) {
        final String methodName = "getByLocationCode() : ";
        logger.info(methodName + " called for location " + locationCode);
        ZoneInterface zone = null;
        if(locationCode != null){
            zone = zoneRepository.findByCode(locationCode);
        }
        return zone;
    }

    @Override
    public List<ZoneInterface> getByDivisionId(long divisionId) {
        final String methodName = "getByDivisionId() : ";
        logger.info(methodName + "called");
        return zoneRepository.findByDivisionId(divisionId);
    }

    @Override
    public ZoneInterface getByShortCode(String shortCode) {
        final String methodName = "getByShortCode() : ";
        logger.info(methodName + "called");
        ZoneInterface zoneInterface = null;
        if(shortCode != null){
            zoneInterface = zoneRepository.findByCode(shortCode);
        }
        return zoneInterface;
    }
}
