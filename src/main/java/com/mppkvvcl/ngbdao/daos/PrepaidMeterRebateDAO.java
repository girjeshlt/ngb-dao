package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PrepaidMeterRebateDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PrepaidMeterRebateRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PrepaidMeterRebateInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PrepaidMeterRebateDAO implements PrepaidMeterRebateDAOInterface<PrepaidMeterRebateInterface>{

    private Logger logger = GlobalResources.getLogger(PrepaidMeterRebateDAO.class);

    @Autowired
    private PrepaidMeterRebateRepository prepaidMeterRebateRepository;

    @Override
    public PrepaidMeterRebateInterface getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        PrepaidMeterRebateInterface prepaidMeterRebateInterface = null;
        if(date != null){
            prepaidMeterRebateInterface = prepaidMeterRebateRepository.findByDate(date);
        }
        return prepaidMeterRebateInterface;
    }

    @Override
    public PrepaidMeterRebateInterface add(PrepaidMeterRebateInterface prepaidMeterRebateInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PrepaidMeterRebateInterface addedPrepaidMeterRebateInterface = null;
        if(prepaidMeterRebateInterface != null){
            addedPrepaidMeterRebateInterface = prepaidMeterRebateRepository.save(prepaidMeterRebateInterface);
        }
        return addedPrepaidMeterRebateInterface;
    }

    @Override
    public PrepaidMeterRebateInterface update(PrepaidMeterRebateInterface prepaidMeterRebateInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PrepaidMeterRebateInterface updatedPrepaidMeterRebateInterface = null;
        if(prepaidMeterRebateInterface != null){
            updatedPrepaidMeterRebateInterface = prepaidMeterRebateRepository.save(prepaidMeterRebateInterface);
        }
        return updatedPrepaidMeterRebateInterface;
    }

    @Override
    public PrepaidMeterRebateInterface get(PrepaidMeterRebateInterface prepaidMeterRebateInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PrepaidMeterRebateInterface fetchedPrepaidMeterRebateInterface = null;
        if(prepaidMeterRebateInterface != null){
            fetchedPrepaidMeterRebateInterface = prepaidMeterRebateRepository.findOne(prepaidMeterRebateInterface.getId());
        }
        return fetchedPrepaidMeterRebateInterface;
    }

    @Override
    public PrepaidMeterRebateInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        PrepaidMeterRebateInterface prepaidMeterRebateInterface = null;
        prepaidMeterRebateInterface = prepaidMeterRebateRepository.findOne(id);
        return prepaidMeterRebateInterface;
    }
}
