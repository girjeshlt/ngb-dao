package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SecurityDepositPostedDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SecurityDepositPostedRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.SecurityDeposit;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositPostedInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */

@Service
public class SecurityDepositPostedDAO implements SecurityDepositPostedDAOInterface<SecurityDepositPostedInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(SecurityDepositPostedDAO.class);

    /**
     * Requesting spring to get singleton SecurityDepositPostedRepository object.
     */
    @Autowired
    private SecurityDepositPostedRepository securityDepositPostedRepository ;

    @Override
    public SecurityDepositPostedInterface add(SecurityDepositPostedInterface securityDepositToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SecurityDepositPostedInterface securityDeposit = null;
        if (securityDepositToAdd != null){
            securityDeposit = securityDepositPostedRepository.save(securityDepositToAdd);
        }
        return securityDeposit;
    }

    @Override
    public SecurityDepositPostedInterface update(SecurityDepositPostedInterface securityDepositInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SecurityDepositPostedInterface updatedSecurityDeposit = null;
        if (securityDepositInterface != null){
            updatedSecurityDeposit = securityDepositPostedRepository.save(securityDepositInterface);
        }
        return updatedSecurityDeposit;
    }

    @Override
    public SecurityDepositPostedInterface get(SecurityDepositPostedInterface securityDepositInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SecurityDepositPostedInterface existingReadMaster = null;
        if (securityDepositInterface != null){
            existingReadMaster = securityDepositPostedRepository.findOne(securityDepositInterface.getId());
        }
        return existingReadMaster;
    }

    @Override
    public SecurityDepositPostedInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SecurityDepositPostedInterface securityDepositInterface = null;
        securityDepositInterface = securityDepositPostedRepository.findOne(id);
        return securityDepositInterface;
    }

    @Override
    public List<SecurityDepositPostedInterface> getBySecurityDepositId(long securityDepositId) {
        final String methodName = "getBySecurityDepositId() : ";
        logger.info(methodName + "called");
        List<SecurityDepositPostedInterface> securityDepositPosted = null;
        securityDepositPosted = securityDepositPostedRepository.findBySecurityDepositId(securityDepositId);
        return securityDepositPosted;
    }

    @Override
    public List<SecurityDepositPostedInterface> getByBillMonth(String billMonth) {
        final String methodName = "getByBillMonth() : ";
        logger.info(methodName + "called");
        List<SecurityDepositPostedInterface> securityDepositPosted = null;
        if(billMonth != null){
            securityDepositPosted = securityDepositPostedRepository.findByBillMonth(billMonth);
        }
        return securityDepositPosted;
    }

    public List<SecurityDepositPostedInterface> add(List<SecurityDepositPostedInterface> securityDepositPostedToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<SecurityDepositPostedInterface> securityDepositPostedSaved = null;
        if (securityDepositPostedToAdd != null && securityDepositPostedToAdd.size() > 0){
            securityDepositPostedSaved = new ArrayList<>();
            for(SecurityDepositPostedInterface securityDepositPostedInterface : securityDepositPostedToAdd){
                SecurityDepositPostedInterface insertedSecurityDepositPosted = add(securityDepositPostedInterface);
                securityDepositPostedSaved.add(insertedSecurityDepositPosted);
            }
        }
        return securityDepositPostedSaved;
    }
}
