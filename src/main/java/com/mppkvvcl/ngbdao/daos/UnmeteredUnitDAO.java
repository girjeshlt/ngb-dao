package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.UnmeteredUnitDAOInterface;
import com.mppkvvcl.ngbdao.repositories.UnmeteredUnitRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.UnmeteredUnitInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnmeteredUnitDAO implements UnmeteredUnitDAOInterface<UnmeteredUnitInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(UnmeteredUnitDAO.class);

    /**
     * Requesting spring to get singleton  UnmeteredUnitDAO object.
     */
    @Autowired
    private UnmeteredUnitRepository unmeteredUnitRepository;

    @Override
    public UnmeteredUnitInterface getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        UnmeteredUnitInterface unmeteredUnit = null;
        unmeteredUnit = unmeteredUnitRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return unmeteredUnit;
    }

    @Override
    public UnmeteredUnitInterface add(UnmeteredUnitInterface unmeteredUnit) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        UnmeteredUnitInterface insertedUnmeteredUnit = null;
        if(unmeteredUnit != null){
            insertedUnmeteredUnit = unmeteredUnitRepository.save(unmeteredUnit);
        }
        return insertedUnmeteredUnit;
    }

    @Override
    public UnmeteredUnitInterface update(UnmeteredUnitInterface unmeteredUnit) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        UnmeteredUnitInterface updatedUnmeteredUnit = null;
        if(unmeteredUnit != null){
            updatedUnmeteredUnit = unmeteredUnitRepository.save(unmeteredUnit);
        }
        return updatedUnmeteredUnit;
    }

    @Override
    public UnmeteredUnitInterface get(UnmeteredUnitInterface unmeteredUnit) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        UnmeteredUnitInterface fetchedUnmeteredUnit = null;
        if(unmeteredUnit != null){
            fetchedUnmeteredUnit = unmeteredUnitRepository.findOne(unmeteredUnit.getId());
        }
        return fetchedUnmeteredUnit;
    }

    @Override
    public UnmeteredUnitInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        UnmeteredUnitInterface unmeteredUnit = null;
        unmeteredUnit = unmeteredUnitRepository.findOne(id);
        return unmeteredUnit;
    }
}
