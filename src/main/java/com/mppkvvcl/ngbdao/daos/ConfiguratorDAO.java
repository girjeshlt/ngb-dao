package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.repositories.ConfiguratorRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConfiguratorInterface;
import com.mppkvvcl.ngbdao.interfaces.ConfiguratorDAOInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConfiguratorDAO implements ConfiguratorDAOInterface<ConfiguratorInterface> {

    @Autowired
    private ConfiguratorRepository configuratorRepository;

    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(ConfiguratorDAO.class);

    @Override
    public ConfiguratorInterface getByCode(String code) {
        final String methodName = "getByCode() ";
        logger.info(methodName + "called");
        ConfiguratorInterface configurator = null;
        if(code != null){
            configurator = configuratorRepository.findByCode(code);
        }else{
            logger.error(methodName + "input parameter can not be null");
        }
        return configurator;
    }

    @Override
    public ConfiguratorInterface add(ConfiguratorInterface configurator) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConfiguratorInterface insertedConfigurator = null;
        if(configurator != null){
            insertedConfigurator = configuratorRepository.save(configurator);
        }
        return insertedConfigurator;
    }

    @Override
    public ConfiguratorInterface get(ConfiguratorInterface configurator) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConfiguratorInterface existingConfigurator = null;
        if(configurator != null){
            existingConfigurator = configuratorRepository.findOne(configurator.getId());
        }
        return existingConfigurator;
    }

    @Override
    public ConfiguratorInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConfiguratorInterface configurator = null;
        if(id > 0){
          configurator = configuratorRepository.findOne(id);
        }
        return configurator;
    }

    @Override
    public ConfiguratorInterface update(ConfiguratorInterface configurator) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConfiguratorInterface updatedConfigurator = null;
        if(configurator != null){
            updatedConfigurator = configuratorRepository.save(configurator);
        }
        return updatedConfigurator;
    }
}
