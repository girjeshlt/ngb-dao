package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;
import com.mppkvvcl.ngbdao.interfaces.AgricultureBill6MonthlyDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AgricultureBill6MonthlyRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class AgricultureBill6MonthlyDAO implements AgricultureBill6MonthlyDAOInterface<AgricultureBill6MonthlyInterface> {

    /**
     * Requesting spring to get singleton AgricultureBill6MonthlyRepository object.
     */
    @Autowired
    private AgricultureBill6MonthlyRepository agricultureBill6MonthlyRepository;


    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(AgricultureBill6MonthlyDAO.class);

    @Override
    public AgricultureBill6MonthlyInterface getTopByConsumerNo(String consumerNo) {
        final String methodName = "getTopByConsumerNo() : ";
        logger.info(methodName + "is called");
        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        if(consumerNo != null){
            agricultureBill6Monthly = agricultureBill6MonthlyRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return agricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        agricultureBill6Monthly = agricultureBill6MonthlyRepository.findOne(id);
        return agricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface update(AgricultureBill6MonthlyInterface agricultureBill6Monthly) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AgricultureBill6MonthlyInterface updatedAgricultureBill6Monthly = null;
        if(agricultureBill6Monthly != null){
            updatedAgricultureBill6Monthly = agricultureBill6MonthlyRepository.save(agricultureBill6Monthly);
        }
        return updatedAgricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface get(AgricultureBill6MonthlyInterface agricultureBill6Monthly) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AgricultureBill6MonthlyInterface existingAgricultureBill6Monthly = null;
        if(agricultureBill6Monthly != null){
            existingAgricultureBill6Monthly = agricultureBill6MonthlyRepository.findOne(agricultureBill6Monthly.getId());
        }
        return existingAgricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface add(AgricultureBill6MonthlyInterface agricultureBill6Monthly) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AgricultureBill6MonthlyInterface insertedAgricultureBill6Monthly = null;
        if(agricultureBill6Monthly != null){
            insertedAgricultureBill6Monthly = agricultureBill6MonthlyRepository.save(agricultureBill6Monthly);
        }
        return insertedAgricultureBill6Monthly;
    }

    @Override
    public AgricultureBill6MonthlyInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        if(consumerNo != null){
            agricultureBill6Monthly = agricultureBill6MonthlyRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return agricultureBill6Monthly;
    }
}
