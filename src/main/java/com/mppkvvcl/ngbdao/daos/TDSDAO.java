package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TDSDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TDSRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TDSInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TDSDAO implements TDSDAOInterface<TDSInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(TDSDAO.class);

    @Autowired
    private TDSRepository tdsRepository ;

    @Override
    public TDSInterface getBySecurityDepositInterestId(long securityDepositInterestId) {
        final String methodName = "getBySecurityDepositInterestId() : ";
        logger.info(methodName + "called");
        TDSInterface tdsInterface = null;
        tdsInterface = tdsRepository.findBySecurityDepositInterestId(securityDepositInterestId);
        return tdsInterface;
    }

    @Override
    public TDSInterface add(TDSInterface tdsInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TDSInterface insertedTDS = null;
        if (tdsInterface != null){
            insertedTDS = tdsRepository.save(tdsInterface);
        }
        return insertedTDS;
    }

    @Override
    public TDSInterface update(TDSInterface tdsInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TDSInterface updatedTDS = null;
        if (tdsInterface != null){
            updatedTDS = tdsRepository.save(tdsInterface);
        }
        return updatedTDS;
    }

    @Override
    public TDSInterface get(TDSInterface tdsInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TDSInterface existingTDS = null;
        if (tdsInterface != null){
            existingTDS = tdsRepository.findOne(tdsInterface.getId());
        }
        return existingTDS;
    }

    @Override
    public TDSInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TDSInterface existingTDS = null;
        existingTDS = tdsRepository.findOne(id);
        return existingTDS;
    }
}
