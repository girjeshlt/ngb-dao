package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ReadMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ReadMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class ReadMasterDAO implements ReadMasterDAOInterface<ReadMasterInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(ReadMasterDAO.class);

    @Autowired
    ReadMasterRepository readMasterRepository;


    @Override
    public ReadMasterInterface add(ReadMasterInterface readMaster) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ReadMasterInterface insertedReadMaster = null;
        if (readMaster != null){
            insertedReadMaster = readMasterRepository.save(readMaster);
        }
        return insertedReadMaster;
    }

    @Override
    public ReadMasterInterface update(ReadMasterInterface readMaster) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ReadMasterInterface updatedReadMaster = null;
        if (readMaster != null){
            updatedReadMaster = readMasterRepository.save(readMaster);
        }
        return updatedReadMaster;
    }

    @Override
    public ReadMasterInterface get(ReadMasterInterface readMaster) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ReadMasterInterface existingReadMaster = null;
        if (readMaster != null){
            existingReadMaster = readMasterRepository.findOne(readMaster.getId());
        }
        return existingReadMaster;
    }

    @Override
    public ReadMasterInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ReadMasterInterface existingReadMaster = null;
        if(id != 0){
            existingReadMaster = readMasterRepository.findOne(id);
        }
        return existingReadMaster;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> fetchedReadMaster = null;
        if(consumerNo != null){
            fetchedReadMaster = readMasterRepository.findByConsumerNo(consumerNo);
        }
        return fetchedReadMaster;
    }

    @Override
    public ReadMasterInterface getTopByConsumerNoOrderByReadingDateDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByReadingDateDesc() : ";
        logger.info(methodName + "called");
        ReadMasterInterface fetchedReadMaster = null;
        if(consumerNo != null){
            fetchedReadMaster = readMasterRepository.findTopByConsumerNoOrderByReadingDateDesc(consumerNo);
        }
        return fetchedReadMaster;
    }

    @Override
    public ReadMasterInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        ReadMasterInterface fetchedReadMaster = null;
        if(consumerNo != null){
            fetchedReadMaster = readMasterRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return fetchedReadMaster;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag) {
        final String methodName = "getByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> fetchedReadMaster = null;
        if(consumerNo != null && billMonth != null && replacementFlag != null){
            fetchedReadMaster = readMasterRepository.findByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc(consumerNo , billMonth , replacementFlag);
        }
        return fetchedReadMaster;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth) {
        final String methodName = "getByConsumerNoAndBillMonthOrderByIdAsc() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> fetchedReadMaster = null;
        if(consumerNo != null && billMonth != null){
            fetchedReadMaster = readMasterRepository.findByConsumerNoAndBillMonthOrderByIdAsc(consumerNo , billMonth);
        }
        return fetchedReadMaster;
    }

    @Override
    public ReadMasterInterface getByConsumerNoAndReplacementFlag(String consumerNo, String replacementFlag) {
        final String methodName = "getByConsumerNoAndReplacementFlag() : ";
        logger.info(methodName + "called");
        ReadMasterInterface fetchedReadMaster = null;
        if(consumerNo != null){
            fetchedReadMaster = readMasterRepository.findByConsumerNoAndReplacementFlag(consumerNo,replacementFlag);
        }
        return fetchedReadMaster;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag, Boolean usedOnBill) {
        final String methodName = "getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> fetchedReadMaster = null;
        if(consumerNo != null){
            fetchedReadMaster = readMasterRepository.findByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(consumerNo,billMonth,replacementFlag,usedOnBill);
        }
        return fetchedReadMaster;
    }

    @Override
    public List<ReadMasterInterface> getTop2ByConsumerNoAndBillMonthOrderByIdDesc(String consumerNo, String billMonth) {
        final String methodName = "getTop2ByConsumerNoAndBillMonthOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> readMasterInterfaces = null;
        if(consumerNo != null && billMonth != null ){
            readMasterInterfaces = readMasterRepository.findTop2ByConsumerNoAndBillMonthOrderByIdDesc(consumerNo,billMonth);
        }
        return  readMasterInterfaces;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoOrderByBillMonthDescendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByBillMonthDescendingWithPageable() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> readMasterInterfaces = null;
        if(consumerNo != null && pageable != null){
            readMasterInterfaces = readMasterRepository.findByConsumerNoOrderByBillMonthDescendingWithPageable(consumerNo,pageable);
        }
        return readMasterInterfaces;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoOrderByBillMonthAscendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByBillMonthAscendingWithPageable() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> readMasterInterfaces = null;
        if(consumerNo != null && pageable != null){
            readMasterInterfaces = readMasterRepository.findByConsumerNoOrderByBillMonthAscendingWithPageable(consumerNo,pageable);
        }
        return readMasterInterfaces;
    }

    @Override
    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = readMasterRepository.countByConsumerNo(consumerNo);
        }
        return count;
    }

    @Override
    public List<ReadMasterInterface> getByConsumerNoAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String replacementFlag, Boolean usedOnBill) {
        final String methodName = "getByConsumerNoAndReplacementFlagAndUsedOnBillOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ReadMasterInterface> readMasterInterfaces = null;
        if (!StringUtils.isEmpty(consumerNo) && !StringUtils.isEmpty(replacementFlag)) {
            readMasterInterfaces = readMasterRepository.findByConsumerNoAndReplacementFlagAndUsedOnBillOrderByIdDesc(consumerNo, replacementFlag, usedOnBill);
        }
        return readMasterInterfaces;
    }
}
