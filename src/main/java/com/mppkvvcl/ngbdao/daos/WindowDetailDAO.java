package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.WindowDetailInterface;
import com.mppkvvcl.ngbdao.interfaces.WindowDetailDAOInterface;
import com.mppkvvcl.ngbdao.repositories.WindowDetailRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class WindowDetailDAO implements WindowDetailDAOInterface<WindowDetailInterface> {

    Logger logger = GlobalResources.getLogger(WindowDetailDAO.class);

    @Autowired
    WindowDetailRepository windowDetailRepository;

    @Override
    public WindowDetailInterface add(WindowDetailInterface windowDetail) {
        final String methodName = "add() :  ";
        logger.info(methodName+"clicked");
        WindowDetailInterface insertedWindowDetail = null;
        if (windowDetail != null){
            insertedWindowDetail = windowDetailRepository.save(windowDetail);
        }
        return insertedWindowDetail;
    }

    @Override
    public WindowDetailInterface update(WindowDetailInterface windowDetail) {
        final String methodName = "update() :  ";
        logger.info(methodName+"clicked");
        WindowDetailInterface updateWindowDetail = null;
        if (windowDetail != null){
            updateWindowDetail = windowDetailRepository.save(windowDetail);
        }
        return updateWindowDetail;
    }

    @Override
    public WindowDetailInterface get(WindowDetailInterface windowDetail) {
        final String methodName = "get() :  ";
        logger.info(methodName+"clicked");
        WindowDetailInterface getWindowDetail = null;
        if (windowDetail != null){
            getWindowDetail = windowDetailRepository.findOne(windowDetail.getId());
        }
        return getWindowDetail;
    }

    @Override
    public WindowDetailInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName+"clicked");
        WindowDetailInterface windowDetail = null;
        if (id > 0 ){
            windowDetail = windowDetailRepository.findOne(id);
        }
        return windowDetail;
    }

    @Override
    public List<WindowDetailInterface> getByLocationCode(String locationCode) {
        final String methodName = "getByLocationCode() :  ";
        logger.info(methodName+"clicked");
        List<WindowDetailInterface> windowDetails =  null;
        if (locationCode != null){
            windowDetails = windowDetailRepository.findByLocationCode(locationCode);
        }
        return windowDetails;
    }
}
