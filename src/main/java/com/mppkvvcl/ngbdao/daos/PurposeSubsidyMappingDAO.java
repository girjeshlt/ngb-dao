package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PurposeSubsidyMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PurposeSubsidyMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubsidyMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PurposeSubsidyMappingDAO implements PurposeSubsidyMappingDAOInterface<PurposeSubsidyMappingInterface> {

    private Logger logger = GlobalResources.getLogger(PurposeSubsidyMappingDAO.class);

    @Autowired
    private PurposeSubsidyMappingRepository purposeSubsidyMappingRepository;


    @Override
    public PurposeSubsidyMappingInterface add(PurposeSubsidyMappingInterface purposeSubsidyMappingInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PurposeSubsidyMappingInterface insertedPurposeSubsidyMappingInterface = null;
        if(purposeSubsidyMappingInterface != null){
            insertedPurposeSubsidyMappingInterface = purposeSubsidyMappingRepository.save(purposeSubsidyMappingInterface);
        }
        return insertedPurposeSubsidyMappingInterface;
    }

    @Override
    public PurposeSubsidyMappingInterface update(PurposeSubsidyMappingInterface purposeSubsidyMappingInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PurposeSubsidyMappingInterface updatedPurposeSubsidyMappingInterface = null;
        if(purposeSubsidyMappingInterface != null){
            updatedPurposeSubsidyMappingInterface = purposeSubsidyMappingRepository.save(purposeSubsidyMappingInterface);
        }
        return updatedPurposeSubsidyMappingInterface;
    }

    @Override
    public PurposeSubsidyMappingInterface get(PurposeSubsidyMappingInterface purposeSubsidyMappingInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PurposeSubsidyMappingInterface fetchedPurposeSubsidyMappingInterface = null;
        if(purposeSubsidyMappingInterface != null){
            fetchedPurposeSubsidyMappingInterface = purposeSubsidyMappingRepository.findOne(purposeSubsidyMappingInterface.getId());
        }
        return fetchedPurposeSubsidyMappingInterface;
    }

    @Override
    public PurposeSubsidyMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        PurposeSubsidyMappingInterface purposeSubsidyMappingInterface = purposeSubsidyMappingRepository.findOne(id);
        return purposeSubsidyMappingInterface;
    }

    @Override
    public List<PurposeSubsidyMappingInterface> getBySubcategoryCodeAndPurposeOfInstallationIdAndEffectiveStartDateAndEffectiveEndDate(long subcategoryCode, long purposeOfInstallationId, Date date) {
        final String methodName = "getBySubcategoryCodeAndPurposeOfInstallationIdAndEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces = null;
        if(date != null){
            purposeSubsidyMappingInterfaces = purposeSubsidyMappingRepository.findBySubcategoryCodeAndPurposeOfInstallationIdAndEffectiveStartDateAndEffectiveEndDate(subcategoryCode,purposeOfInstallationId,date);
        }
        return purposeSubsidyMappingInterfaces;
    }

    @Override
    public List<PurposeSubsidyMappingInterface> getBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate(long subcategoryCode, Date date) {
        final String methodName = "getBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces = null;
        if(date != null){
            purposeSubsidyMappingInterfaces = purposeSubsidyMappingRepository.findBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate(subcategoryCode,date);
        }
        return purposeSubsidyMappingInterfaces;
    }
}
