package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.BillCalculationLineDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BillCalculationLineRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BillCalculationLineInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PREETESH on 12/12/2017.
 */
@Service
public class BillCalculationLineDAO implements BillCalculationLineDAOInterface<BillCalculationLineInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(BillCalculationLineDAO.class);

    /**
     * Requesting spring to get singleton  BillCalculationLineRepository object.
     */
    @Autowired
    private BillCalculationLineRepository billCalculationLineRepository;

    @Override
    public List<BillCalculationLineInterface> getByBillId(long billId) {
        final String methodName = "getByBillId() : ";
        logger.info(methodName + "is called");
        List<BillCalculationLineInterface> billCalculationLineInterface = null;
        if(billId > 0 ){
            billCalculationLineInterface = billCalculationLineRepository.findByBillId(billId);
        }
        return billCalculationLineInterface;
    }

    @Override
    public BillCalculationLineInterface get(BillCalculationLineInterface billCalculationLineInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        BillCalculationLineInterface existingBillCalculationLineInterface = null;
        if(billCalculationLineInterface != null){
            existingBillCalculationLineInterface = billCalculationLineRepository.findOne(billCalculationLineInterface.getId());
        }
        return existingBillCalculationLineInterface;
    }

    @Override
    public BillCalculationLineInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        BillCalculationLineInterface billCalculationLineInterface = null;
        billCalculationLineInterface = billCalculationLineRepository.findOne(id);
        return billCalculationLineInterface;
    }

    @Override
    public BillCalculationLineInterface add(BillCalculationLineInterface billCalculationLineInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        BillCalculationLineInterface insertedBillCalculationLine = null;
        if(billCalculationLineInterface != null){
            insertedBillCalculationLine = billCalculationLineRepository.save(billCalculationLineInterface);
        }
        return insertedBillCalculationLine;
    }

    @Override
    public List<BillCalculationLineInterface> addAll(List<BillCalculationLineInterface> billCalculationLineInterfaces) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<BillCalculationLineInterface> insertedBillCalculationLineInterfaces = null;
        if(billCalculationLineInterfaces != null && billCalculationLineInterfaces.size() > 0){
            insertedBillCalculationLineInterfaces = new ArrayList<>();
            for(BillCalculationLineInterface billCalculationLineInterface : billCalculationLineInterfaces){
                if(billCalculationLineInterface != null) {
                    BillCalculationLineInterface savedBillCalculationLineInterface = billCalculationLineRepository.save(billCalculationLineInterface);
                    if(savedBillCalculationLineInterface != null){
                        insertedBillCalculationLineInterfaces.add(savedBillCalculationLineInterface);
                    }
                }
            }
        }
        return insertedBillCalculationLineInterfaces;
    }

    @Override
    public BillCalculationLineInterface update(BillCalculationLineInterface billCalculationLineInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called ");
        BillCalculationLineInterface updatedBillCalculationLine = null;
        if(billCalculationLineInterface != null){
            updatedBillCalculationLine = billCalculationLineRepository.save(billCalculationLineInterface);
        }
        return updatedBillCalculationLine;
    }
}
