package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterMakeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterMakeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMakeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterMakeDAO implements MeterMakeDAOInterface<MeterMakeInterface> {

    Logger logger = GlobalResources.getLogger(MeterMakeDAO.class);

    @Autowired
    MeterMakeRepository meterMakeRepository ;

    @Override
    public MeterMakeInterface add(MeterMakeInterface meterMake) {
        final String methodName =  "add() :  ";
        logger.info(methodName+"clicked");
        MeterMakeInterface insertedMeterMake = null;
        if (meterMake != null){
            insertedMeterMake = meterMakeRepository.save(meterMake);
        }
        return insertedMeterMake;
    }

    @Override
    public MeterMakeInterface update(MeterMakeInterface meterMake) {
        final String methodName =  "update() :  ";
        logger.info(methodName+"clicked");
        MeterMakeInterface updatedMeterMake = null;
        if (meterMake != null){
            updatedMeterMake = meterMakeRepository.save(meterMake);
        }
        return updatedMeterMake;
    }

    @Override
    public MeterMakeInterface get(MeterMakeInterface meterMake) {
        final String methodName =  "get() :  ";
        logger.info(methodName+"clicked");
        MeterMakeInterface getMeterMake = null;
        if (meterMake != null){
            getMeterMake = meterMakeRepository.findOne(meterMake.getId());
        }
        return getMeterMake;
    }

    @Override
    public MeterMakeInterface getById(long id) {
        final String methodName =  "getById() :  ";
        logger.info(methodName+"clicked");
        MeterMakeInterface meterMake = null;
        if (id > 0){
            meterMake = meterMakeRepository.findOne(id);
        }
        return meterMake;
    }

    @Override
    public List<? extends MeterMakeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return meterMakeRepository.findAll();
    }
}
