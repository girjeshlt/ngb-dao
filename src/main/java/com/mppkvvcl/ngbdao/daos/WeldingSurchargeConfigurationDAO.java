package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.WeldingSurchargeConfigurationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.WeldingSurchargeConfigurationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.WeldingSurchargeConfigurationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeldingSurchargeConfigurationDAO implements WeldingSurchargeConfigurationDAOInterface<WeldingSurchargeConfigurationInterface>{

    private Logger logger = GlobalResources.getLogger(WeldingSurchargeConfigurationDAO.class);

    @Autowired
    private WeldingSurchargeConfigurationRepository weldingSurchargeConfigurationRepository;

    public WeldingSurchargeConfigurationInterface getByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode){
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface = weldingSurchargeConfigurationRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return weldingSurchargeConfigurationInterface;
    }

    @Override
    public WeldingSurchargeConfigurationInterface add(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        WeldingSurchargeConfigurationInterface insertedWeldingSurchargeConfigurationInterface = null;
        if(weldingSurchargeConfigurationInterface != null){
            insertedWeldingSurchargeConfigurationInterface = weldingSurchargeConfigurationRepository.save(weldingSurchargeConfigurationInterface);
        }
        return insertedWeldingSurchargeConfigurationInterface;
    }

    @Override
    public WeldingSurchargeConfigurationInterface update(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        WeldingSurchargeConfigurationInterface updatedWeldingSurchargeConfigurationInterface = null;
        if(weldingSurchargeConfigurationInterface != null){
            updatedWeldingSurchargeConfigurationInterface = weldingSurchargeConfigurationRepository.save(weldingSurchargeConfigurationInterface);
        }
        return updatedWeldingSurchargeConfigurationInterface;
    }

    @Override
    public WeldingSurchargeConfigurationInterface get(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        WeldingSurchargeConfigurationInterface fetchedWeldingSurchargeConfigurationInterface = null;
        if(weldingSurchargeConfigurationInterface != null){
            fetchedWeldingSurchargeConfigurationInterface = weldingSurchargeConfigurationRepository.findOne(weldingSurchargeConfigurationInterface.getId());
        }
        return fetchedWeldingSurchargeConfigurationInterface;
    }

    @Override
    public WeldingSurchargeConfigurationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface = weldingSurchargeConfigurationRepository.findOne(id);
        return weldingSurchargeConfigurationInterface;
    }
}
