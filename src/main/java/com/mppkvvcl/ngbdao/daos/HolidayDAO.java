package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.HolidayDAOInterface;
import com.mppkvvcl.ngbdao.repositories.HolidayRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.HolidayInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 10/3/2017.
 */
@Service
public class HolidayDAO implements HolidayDAOInterface<HolidayInterface> {


    /**
     * Requesting spring to get singleton  Repository object.
     */
    @Autowired
    HolidayRepository holidayRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(HolidayDAO.class);

    @Override
    public HolidayInterface get(HolidayInterface holiday) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        HolidayInterface existingGroup = null;
        if(holiday != null){
            existingGroup = holidayRepository.findOne(holiday.getId());
        }
        return existingGroup;
    }

    @Override
    public HolidayInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        HolidayInterface group = null;
        group = holidayRepository.findOne(id);
        return group;
    }

    @Override
    public HolidayInterface add(HolidayInterface group) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        HolidayInterface insertedGroup = null;
        if(group != null){
            insertedGroup = holidayRepository.save(group);
        }
        return insertedGroup;
    }

    @Override
    public HolidayInterface update(HolidayInterface holiday) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        HolidayInterface updatedHoliday = null;
        if(holiday != null){
            updatedHoliday = holidayRepository.save(holiday);
        }
        return updatedHoliday;
    }

    @Override
    public HolidayInterface getByDate(Date date) {
        final String methodName = "getByDate() :  ";
        logger.info(methodName+"clicked");
        HolidayInterface holiday =  null;
        if (date != null){
            holiday = holidayRepository.findByDate(date);
        }
        return holiday;
    }
}