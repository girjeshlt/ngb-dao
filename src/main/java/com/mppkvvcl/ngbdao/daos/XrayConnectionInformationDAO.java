package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionInformationInterface;
import com.mppkvvcl.ngbdao.interfaces.XrayConnectionInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.XrayConnectionInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class XrayConnectionInformationDAO implements XrayConnectionInformationDAOInterface<XrayConnectionInformationInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(XrayConnectionInformationDAO.class);

    @Autowired
    XrayConnectionInformationRepository xrayConnectionInformationRepository = null;
    
    @Override
    public XrayConnectionInformationInterface add(XrayConnectionInformationInterface xrayConnectionInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        XrayConnectionInformationInterface insertedXrayConnectionInformation = null;
        if (xrayConnectionInformation != null){
            insertedXrayConnectionInformation = xrayConnectionInformationRepository.save(xrayConnectionInformation);
        }
        return insertedXrayConnectionInformation;
    }

    @Override
    public XrayConnectionInformationInterface update(XrayConnectionInformationInterface xrayConnectionInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        XrayConnectionInformationInterface updatedXrayConnectionInformation = null;
        if (xrayConnectionInformation != null){
            updatedXrayConnectionInformation = xrayConnectionInformationRepository.save(xrayConnectionInformation);
        }
        return updatedXrayConnectionInformation;
    }

    @Override
    public XrayConnectionInformationInterface get(XrayConnectionInformationInterface xrayConnectionInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        XrayConnectionInformationInterface existingXrayConnectionInformation = null;
        if (xrayConnectionInformation != null){
            existingXrayConnectionInformation = xrayConnectionInformationRepository.findOne(xrayConnectionInformation.getId());
        }
        return existingXrayConnectionInformation;
    }

    @Override
    public XrayConnectionInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        XrayConnectionInformationInterface existingXrayConnectionInformation = null;
        if(id != 0){
            existingXrayConnectionInformation = xrayConnectionInformationRepository.findOne(id);
        }
        return existingXrayConnectionInformation;    }

    @Override
    public List<XrayConnectionInformationInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        if(consumerNo != null){
            fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByConsumerNo(consumerNo);
        }
        return fetchedXrayConnectionInformation;
    }

    @Override
    public List<XrayConnectionInformationInterface> getByConsumerNoAndStatus(String consumerNo, String status) {
        final String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        if(consumerNo != null && status != null){
            fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByConsumerNoAndStatus(consumerNo ,status);
        }
        return fetchedXrayConnectionInformation;
    }

    @Override
    public List<XrayConnectionInformationInterface> getByXrayLoadBetweenAndStatus(BigDecimal startLoad, BigDecimal endLoad, String status) {
        final String methodName = "getByXrayLoadBetweenAndStatus() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        if(startLoad != null && endLoad != null && status != null){
            fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByXrayLoadBetweenAndStatus(startLoad , endLoad , status);
        }
        return fetchedXrayConnectionInformation;
    }

    @Override
    public List<XrayConnectionInformationInterface> getByStatus(String status) {
        final String methodName = "getByStatus() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        if(status != null){
            fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByStatus(status);
        }
        return fetchedXrayConnectionInformation;
    }

    @Override
    public List<XrayConnectionInformationInterface> getByNoOfDentalXrayMachineBetween(Long start, Long end) {
        final String methodName = "getByNoOfDentalXrayMachineBetween() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByNoOfDentalXrayMachineBetween(start , end);
        return fetchedXrayConnectionInformation;
    }

    @Override
    public List<XrayConnectionInformationInterface> getByNoOfSinglePhaseXrayMachineBetween(Long start, Long end) {
        final String methodName = "getByNoOfSinglePhaseXrayMachineBetween() : ";
        logger.info(methodName + "called");
        List<XrayConnectionInformationInterface> fetchedXrayConnectionInformation = null;
        fetchedXrayConnectionInformation = xrayConnectionInformationRepository.findByNoOfSinglePhaseXrayMachineBetween(start , end);
        return fetchedXrayConnectionInformation;
    }
}
