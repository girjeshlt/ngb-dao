package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PurposeSubCategoryMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PurposeSubCategoryMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class PurposeSubCategoryMappingDAO implements PurposeSubCategoryMappingDAOInterface<PurposeSubCategoryMappingInterface> {
    @Autowired
    PurposeSubCategoryMappingRepository purposeSubCategoryMappingRepository;
    /**
     *
     */
    Logger logger = GlobalResources.getLogger(PurposeSubCategoryMappingDAO.class);

    @Override
    public PurposeSubCategoryMappingInterface getByPurposeOfInstallationIdAndSubcategoryCode(long purposeOfInstallationId, long subcategoryCode) {
        final String methodName = "getByPurposeOfInstallationIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        PurposeSubCategoryMappingInterface purposeSubCategoryMapping = null;
        if(purposeOfInstallationId > 0 && subcategoryCode > 0){
            purposeSubCategoryMapping = purposeSubCategoryMappingRepository.findByPurposeOfInstallationIdAndSubcategoryCode(purposeOfInstallationId, subcategoryCode);
        }
        return purposeSubCategoryMapping;
    }

    @Override
    public List<PurposeSubCategoryMappingInterface> getByPurposeOfInstallationId(long purposeOfInstallationId) {
        final String methodName = "getByPurposeOfInstallationId() : ";
        logger.info(methodName + "called");
        List<PurposeSubCategoryMappingInterface> purposeSubCategoryMappings = null;
        if(purposeOfInstallationId > 0){
            purposeSubCategoryMappings = purposeSubCategoryMappingRepository.findByPurposeOfInstallationId(purposeOfInstallationId);
        }
        return purposeSubCategoryMappings;
    }

    @Override
    public List<PurposeSubCategoryMappingInterface> getBySubcategoryCode(long subcategoryCode) {
        final String methodName = "getBySubcategoryCode() : ";
        logger.info(methodName + "called");
        List<PurposeSubCategoryMappingInterface> purposeSubCategoryMappings = null;
        if(subcategoryCode > 0){
            purposeSubCategoryMappings = purposeSubCategoryMappingRepository.findBySubcategoryCode(subcategoryCode);
        }
        return purposeSubCategoryMappings;
    }

    @Override
    public PurposeSubCategoryMappingInterface add(PurposeSubCategoryMappingInterface purposeSubCategoryMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PurposeSubCategoryMappingInterface insertedPurposeSubCategoryMapping = null;
        if(purposeSubCategoryMapping != null){
            insertedPurposeSubCategoryMapping = purposeSubCategoryMappingRepository.save(purposeSubCategoryMapping);
        }
        return insertedPurposeSubCategoryMapping;
    }

    @Override
    public PurposeSubCategoryMappingInterface get(PurposeSubCategoryMappingInterface purposeSubCategoryMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PurposeSubCategoryMappingInterface existingPurposeSubCategoryMapping = null;
        if(purposeSubCategoryMapping != null){
            existingPurposeSubCategoryMapping = purposeSubCategoryMappingRepository.findOne(purposeSubCategoryMapping.getId());
        }
        return existingPurposeSubCategoryMapping;
    }

    @Override
    public PurposeSubCategoryMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + " called");
        PurposeSubCategoryMappingInterface purposeSubCategoryMapping = null;
        purposeSubCategoryMapping = purposeSubCategoryMappingRepository.findOne(id);
        return purposeSubCategoryMapping;
    }

    @Override
    public PurposeSubCategoryMappingInterface update(PurposeSubCategoryMappingInterface purposeSubCategoryMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PurposeSubCategoryMappingInterface updatedPurposeSubCategoryMapping = null;
        if(purposeSubCategoryMapping != null){
            updatedPurposeSubCategoryMapping = purposeSubCategoryMappingRepository.save(purposeSubCategoryMapping);
        }
        return updatedPurposeSubCategoryMapping;
    }
}
