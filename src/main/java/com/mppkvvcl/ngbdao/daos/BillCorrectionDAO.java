package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.BillCorrectionDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BillCorrectionRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BillCorrectionInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BillCorrectionDAO implements BillCorrectionDAOInterface<BillCorrectionInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(BillCorrectionDAO.class);

    @Autowired
    private BillCorrectionRepository billCorrectionRepository;


    @Override
    public BillCorrectionInterface get(BillCorrectionInterface billCorrectionInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        BillCorrectionInterface existingBillCorrectionInterface = null;
        if (billCorrectionInterface != null) {
            existingBillCorrectionInterface = billCorrectionRepository.findOne(billCorrectionInterface.getId());
        }
        return existingBillCorrectionInterface;
    }

    @Override
    public BillCorrectionInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        BillCorrectionInterface billCorrectionInterface = null;
        billCorrectionInterface = billCorrectionRepository.findOne(id);
        return billCorrectionInterface;
    }

    @Override
    public BillCorrectionInterface add(BillCorrectionInterface billCorrectionInterfaceToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        BillCorrectionInterface insertedBillCorrectionInterface = null;
        if (billCorrectionInterfaceToAdd != null) {
            insertedBillCorrectionInterface = billCorrectionRepository.save(billCorrectionInterfaceToAdd);
        }
        return insertedBillCorrectionInterface;
    }

    @Override
    public BillCorrectionInterface update(BillCorrectionInterface billCorrectionInterfaceToUpdate) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        BillCorrectionInterface updatedBillCorrectionInterface = null;
        if (billCorrectionInterfaceToUpdate != null) {
            updatedBillCorrectionInterface = billCorrectionRepository.save(billCorrectionInterfaceToUpdate);
        }
        return updatedBillCorrectionInterface;
    }

    @Override
    public List<BillCorrectionInterface> getByConsumerNoAndBillMonthAndDeleted(String consumerNo, String billMonth, boolean deleted) {
        final String methodName = "getByBillMonthAndGroupNoAndDeleted() : ";
        logger.info(methodName + "called");
        List<BillCorrectionInterface> billCorrectionInterfaces = null;
        if (consumerNo != null && billMonth != null) {
            billCorrectionInterfaces = billCorrectionRepository.findByConsumerNoAndBillMonthAndDeleted(consumerNo, billMonth, deleted);
        }
        return billCorrectionInterfaces;
    }
}
