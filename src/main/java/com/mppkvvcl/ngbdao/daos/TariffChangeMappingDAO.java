package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TariffChangeMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffChangeMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffChangeMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by vikas on 10/6/2017.
 */

@Service
public class TariffChangeMappingDAO implements TariffChangeMappingDAOInterface<TariffChangeMappingInterface> {


    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(TariffChangeMappingDAO.class);

    @Autowired
    private TariffChangeMappingRepository tariffChangeMappingRepository = null;

    @Override
    public TariffChangeMappingInterface add(TariffChangeMappingInterface tariffChangeMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TariffChangeMappingInterface insertedTariffDetail = null;
        if (tariffChangeMapping != null){
            insertedTariffDetail = tariffChangeMappingRepository.save(tariffChangeMapping);
        }
        return insertedTariffDetail;    }

    @Override
    public TariffChangeMappingInterface update(TariffChangeMappingInterface tariffChangeMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TariffChangeMappingInterface updatedTariffDetail = null;
        if (tariffChangeMapping != null){
            updatedTariffDetail = tariffChangeMappingRepository.save(tariffChangeMapping);
        }
        return updatedTariffDetail;    }

    @Override
    public TariffChangeMappingInterface get(TariffChangeMappingInterface tariffChangeMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TariffChangeMappingInterface existingTariffDetail = null;
        if (tariffChangeMapping != null){
            existingTariffDetail = tariffChangeMappingRepository.findOne(tariffChangeMapping.getId());
        }
        return existingTariffDetail;
    }

    @Override
    public TariffChangeMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TariffChangeMappingInterface existingTariffDetail = null;
        if(id != 0){
            existingTariffDetail = tariffChangeMappingRepository.findOne(id);
        }
        return existingTariffDetail;
    }

    @Override
    public List<TariffChangeMappingInterface> getByTariffCategory(String tariffCategory) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        List<TariffChangeMappingInterface> existingTariffDetails = null;
        if(tariffCategory != null){
            existingTariffDetails = tariffChangeMappingRepository.findByTariffCategory(tariffCategory);
        }
        return existingTariffDetails;
    }
}
