package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;
import com.mppkvvcl.ngbdao.interfaces.TariffDescriptionDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffDescriptionRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TariffDescriptionDAO implements TariffDescriptionDAOInterface<TariffDescriptionInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(TariffDescriptionDAO.class);

    @Autowired
    private TariffDescriptionRepository tariffDescriptionRepository;

    @Override
    public TariffDescriptionInterface getByTariffCategory(String tariffCategory) {
        final String methodName = "getByTariffCategory() : ";
        logger.info(methodName + "called");
        TariffDescriptionInterface tariffDescription = null;
        if (tariffCategory != null){
            tariffDescription = tariffDescriptionRepository.findByTariffCategory(tariffCategory);
        }
        return tariffDescription;
    }

    @Override
    public TariffDescriptionInterface add(TariffDescriptionInterface tariffDescription) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TariffDescriptionInterface insertedTariffDescription = null;
        if (tariffDescription != null){
            insertedTariffDescription = tariffDescriptionRepository.save(tariffDescription);
        }
        return insertedTariffDescription;
    }

    @Override
    public TariffDescriptionInterface update(TariffDescriptionInterface tariffDescription) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TariffDescriptionInterface updatedTariffDescription = null;
        if (tariffDescription != null){
            updatedTariffDescription = tariffDescriptionRepository.save(tariffDescription);
        }
        return updatedTariffDescription;
    }

    @Override
    public TariffDescriptionInterface get(TariffDescriptionInterface tariffDescription) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TariffDescriptionInterface existingTariffDescription = null;
        if (tariffDescription != null){
            existingTariffDescription = tariffDescriptionRepository.findOne(tariffDescription.getId());
        }
        return existingTariffDescription;
    }

    @Override
    public TariffDescriptionInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TariffDescriptionInterface existingTariffDescription = null;
        existingTariffDescription = tariffDescriptionRepository.findOne(id);
        return existingTariffDescription;
    }

    @Override
    public List<? extends TariffDescriptionInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends TariffDescriptionInterface> tariffs = tariffDescriptionRepository.findAll();
        return tariffs;
    }
}
