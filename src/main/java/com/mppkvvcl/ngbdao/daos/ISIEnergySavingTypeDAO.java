package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ISIEnergySavingTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ISIEnergySavingTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class ISIEnergySavingTypeDAO implements ISIEnergySavingTypeDAOInterface<ISIEnergySavingTypeInterface> {

    Logger logger = GlobalResources.getLogger(ISIEnergySavingTypeDAO.class);

    @Autowired
    ISIEnergySavingTypeRepository isiEnergySavingTypeRepository;

    @Override
    public ISIEnergySavingTypeInterface add(ISIEnergySavingTypeInterface isiEnergySavingType) {
        final String methodName  = "add() :  ";
        logger.info(methodName+"clicked");
        ISIEnergySavingTypeInterface insertedIsiEnergySavingType =  null;
        if (isiEnergySavingType != null){
            insertedIsiEnergySavingType = isiEnergySavingTypeRepository.save(isiEnergySavingType);
        }
        return insertedIsiEnergySavingType;
    }

    @Override
    public ISIEnergySavingTypeInterface update(ISIEnergySavingTypeInterface isiEnergySavingType) {
        final String methodName  = "update() :  ";
        logger.info(methodName+"clicked");
        ISIEnergySavingTypeInterface updatedIsiEnergySavingType =  null;
        if (isiEnergySavingType != null){
            updatedIsiEnergySavingType = isiEnergySavingTypeRepository.save(isiEnergySavingType);
        }
        return updatedIsiEnergySavingType;
    }

    @Override
    public ISIEnergySavingTypeInterface get(ISIEnergySavingTypeInterface isiEnergySavingType) {
        final String methodName  = "get() :  ";
        logger.info(methodName+"clicked");
        ISIEnergySavingTypeInterface getIsiEnergySavingType =  null;
        if (isiEnergySavingType != null){
            getIsiEnergySavingType = isiEnergySavingTypeRepository.findOne(isiEnergySavingType.getId());
        }
        return getIsiEnergySavingType;
    }

    @Override
    public ISIEnergySavingTypeInterface getById(long id) {
        final String methodName  = "getById() :  ";
        logger.info(methodName+"clicked");
        ISIEnergySavingTypeInterface isiEnergySavingType =  null;
        if (id > 0 ){
            isiEnergySavingType = isiEnergySavingTypeRepository.findOne(id);
        }
        return isiEnergySavingType;
    }

    @Override
    public List<ISIEnergySavingTypeInterface> getByType(String type) {
        final String methodName  = "getByType() :  ";
        logger.info(methodName+"clicked");
        List<ISIEnergySavingTypeInterface> isiEnergySavingType =  null;
        if (type != null ){
            isiEnergySavingType = isiEnergySavingTypeRepository.findByType(type);
        }
        return isiEnergySavingType;

    }

    @Override
    public List<? extends ISIEnergySavingTypeInterface> getAll(){
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return isiEnergySavingTypeRepository.findAll();
    }
}
