package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PaymentStagingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PaymentStagingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PaymentStagingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PaymentStagingDAO implements PaymentStagingDAOInterface<PaymentStagingInterface>{

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentStagingDAO.class);

    @Autowired
    private PaymentStagingRepository paymentStagingRepository;


    @Override
    public PaymentStagingInterface add(PaymentStagingInterface paymentStagingInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PaymentStagingInterface insertedPaymentStagingInterface = null;
        if(paymentStagingInterface != null){
            insertedPaymentStagingInterface = paymentStagingRepository.save(paymentStagingInterface);
        }
        return insertedPaymentStagingInterface;
    }

    @Override
    public PaymentStagingInterface update(PaymentStagingInterface paymentStagingInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PaymentStagingInterface updatedPaymentStagingInterface = null;
        if(updatedPaymentStagingInterface != null){
            updatedPaymentStagingInterface = paymentStagingRepository.save(paymentStagingInterface);
        }
        return updatedPaymentStagingInterface;
    }

    @Override
    public PaymentStagingInterface get(PaymentStagingInterface paymentStagingInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PaymentStagingInterface fetchedPaymentStagingInterface = null;
        if(paymentStagingInterface != null){
            fetchedPaymentStagingInterface = paymentStagingRepository.findOne(paymentStagingInterface.getId());
        }
        return fetchedPaymentStagingInterface;
    }

    @Override
    public PaymentStagingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        PaymentStagingInterface paymentStagingInterface = paymentStagingRepository.findOne(id);
        return paymentStagingInterface;
    }


    @Override
    public List<PaymentStagingInterface> getByIvrs(String ivrs) {
        final String methodName = "getByIvrs() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(ivrs != null){
            paymentStagingInterfaces = paymentStagingRepository.findByIvrs(ivrs);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public List<PaymentStagingInterface> getByServiceNo(String serviceNo) {
        final String methodName = "getByServiceNo() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(serviceNo != null){
            paymentStagingInterfaces = paymentStagingRepository.findByServiceNo(serviceNo);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public PaymentStagingInterface getByTransactionId(String transactionId) {
        final String methodName = "getByTransactionId() : ";
        logger.info(methodName + "called");
        PaymentStagingInterface paymentStagingInterface = null;
        if(transactionId != null){
            paymentStagingInterface = paymentStagingRepository.findByTransactionId(transactionId);
        }
        return paymentStagingInterface;
    }

    @Override
    public List<PaymentStagingInterface> getByDateOfTransaction(Date dateOfTransaction) {
        final String methodName = "getByDateOfTransaction() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(dateOfTransaction != null){
            paymentStagingInterfaces = paymentStagingRepository.findByDateOfTransaction(dateOfTransaction);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public List<PaymentStagingInterface> getByStatus(String status) {
        final String methodName = "getByStatus() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(status != null){
            paymentStagingInterfaces = paymentStagingRepository.findByStatus(status);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public List<PaymentStagingInterface> getByLocationCode(String locationCode) {
        final String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(locationCode != null){
            paymentStagingInterfaces = paymentStagingRepository.findByLocationCode(locationCode);
        }
        return paymentStagingInterfaces;
    }


    @Override
    public List<PaymentStagingInterface> getByLocationCodeAndStatus(String locationCode, String status) {
        final String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(locationCode != null && status != null){
            paymentStagingInterfaces = paymentStagingRepository.findByLocationCodeAndStatus(locationCode,status);
        }
        return paymentStagingInterfaces;
    }


    @Override
    public List<PaymentStagingInterface> getByLocationCodeAndDateOfTransactionAndStatus(String locationCode, Date dateOfTransaction, String status) {
        final String methodName = "getByLocationCodeAndDateOfTransactionAndStatus() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(locationCode != null && dateOfTransaction != null && status != null){
            paymentStagingInterfaces = paymentStagingRepository.findByLocationCodeAndDateOfTransactionAndStatus(locationCode, dateOfTransaction, status);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public List<PaymentStagingInterface> getByDateOfTransactionAndStatus(Date dateOfTransaction, String status) {
        final String methodName = "getByDateOfTransactionAndStatus() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(dateOfTransaction != null && status != null){
            paymentStagingInterfaces = paymentStagingRepository.findByDateOfTransactionAndStatus(dateOfTransaction, status);
        }
        return paymentStagingInterfaces;
    }

    @Override
    public List<PaymentStagingInterface> getByIvrsAndStatus(String ivrs, String status) {
        final String methodName = "getByIvrsAndStatus() : ";
        logger.info(methodName + "called");
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        if(ivrs != null && status != null){
            paymentStagingInterfaces = paymentStagingRepository.findByIvrsAndStatus(ivrs, status);
        }
        return paymentStagingInterfaces;
    }
}
