package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerISIEnergySavingMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerISIEnergySavingMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerISIEnergySavingMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class ConsumerISIEnergySavingMappingDAO implements ConsumerISIEnergySavingMappingDAOInterface<ConsumerISIEnergySavingMappingInterface> {
    @Autowired
    ConsumerISIEnergySavingMappingRepository consumerISIEnergySavingMappingRepository;

    Logger logger = GlobalResources.getLogger(ConsumerISIEnergySavingMappingDAO.class);

    @Override
    public ConsumerISIEnergySavingMappingInterface add(ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping) {
        final String methodName  = "add() : ";
        logger.info(methodName+"clicked");
        ConsumerISIEnergySavingMappingInterface insertedConsumerISIEnergySavingMapping= null;
        if (consumerISIEnergySavingMapping != null) {
            insertedConsumerISIEnergySavingMapping  = consumerISIEnergySavingMappingRepository.save(consumerISIEnergySavingMapping);
        }
        return insertedConsumerISIEnergySavingMapping;
    }

    @Override
    public ConsumerISIEnergySavingMappingInterface update(ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping) {
        final String methodName  = "update() : ";
        logger.info(methodName+"clicked");
        ConsumerISIEnergySavingMappingInterface updateConsumerISIEnergySavingMapping= null;
        if (consumerISIEnergySavingMapping != null) {
            updateConsumerISIEnergySavingMapping  = consumerISIEnergySavingMappingRepository.save(consumerISIEnergySavingMapping);
        }
        return updateConsumerISIEnergySavingMapping;
    }

    @Override
    public ConsumerISIEnergySavingMappingInterface get(ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping) {
        final String methodName  = "get() : ";
        logger.info(methodName+"clicked");
        ConsumerISIEnergySavingMappingInterface fetchedConsumerISIEnergySavingMapping= null;
        if (fetchedConsumerISIEnergySavingMapping != null) {
            fetchedConsumerISIEnergySavingMapping  = consumerISIEnergySavingMappingRepository.findOne(fetchedConsumerISIEnergySavingMapping.getId());
        }
        return fetchedConsumerISIEnergySavingMapping;
    }

    @Override
    public ConsumerISIEnergySavingMappingInterface getById(long id) {
        final String methodName  = "getById() : ";
        logger.info(methodName+"clicked");
        ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping= null;
        if (id > 0) {
            consumerISIEnergySavingMapping  = consumerISIEnergySavingMappingRepository.save(consumerISIEnergySavingMapping);
        }
        return consumerISIEnergySavingMapping;
    }
}
