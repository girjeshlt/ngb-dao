package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.GMCDAOInterface;
import com.mppkvvcl.ngbdao.repositories.GMCRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GMCInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class GMCDAO implements GMCDAOInterface<GMCInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(GMCDAO.class);

    @Autowired
    private GMCRepository gmcRepository;


    @Override
    public GMCInterface add(GMCInterface gmc) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        GMCInterface insertedGmc = null;
        if (gmc != null){
            insertedGmc = gmcRepository.save(gmc);
        }
        return insertedGmc;
    }

    @Override
    public GMCInterface update(GMCInterface gmc) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        GMCInterface updatedGmc = null;
        if (gmc != null){
            updatedGmc = gmcRepository.save(gmc);
        }
        return updatedGmc;
    }

    @Override
    public GMCInterface get(GMCInterface gmc) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        GMCInterface existingGmc = null;
        if (gmc != null){
            existingGmc = gmcRepository.findOne(gmc.getId());
        }
        return existingGmc;
    }

    @Override
    public GMCInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        GMCInterface existingGmc = null;
        if(id != 0){
            existingGmc = gmcRepository.findOne(id);
        }
        return existingGmc;
    }

    @Override
    public GMCInterface getBySubcategoryCodeAndTariffId(long subcategoryCode, long tariffId) {
        final String methodName = "getBySubcategoryCodeAndTariffId() : ";
        logger.info(methodName + "called");
        GMCInterface gmcInterface = null;
        gmcInterface = gmcRepository.findBySubcategoryCodeAndTariffId(subcategoryCode,tariffId);
        return gmcInterface;
    }
}
