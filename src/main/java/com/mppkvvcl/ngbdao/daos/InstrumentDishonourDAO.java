package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.InstrumentDishonourDAOInterface;
import com.mppkvvcl.ngbdao.repositories.InstrumentDishonourRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentDishonourInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class InstrumentDishonourDAO implements InstrumentDishonourDAOInterface<InstrumentDishonourInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(InstrumentDishonourDAO.class);

    @Autowired
    InstrumentDishonourRepository instrumentDishonourRepository = null;


    @Override
    public InstrumentDishonourInterface add(InstrumentDishonourInterface instrumentDishonour) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface insertedInstrumentDishonour = null;
        if (instrumentDishonour != null){
            insertedInstrumentDishonour = instrumentDishonourRepository.save(instrumentDishonour);
        }
        return insertedInstrumentDishonour;
    }

    @Override
    public InstrumentDishonourInterface update(InstrumentDishonourInterface instrumentDishonour) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface updatedInstrumentDishonour = null;
        if (instrumentDishonour != null){
            updatedInstrumentDishonour = instrumentDishonourRepository.save(instrumentDishonour);
        }
        return updatedInstrumentDishonour;
    }

    @Override
    public InstrumentDishonourInterface get(InstrumentDishonourInterface instrumentDishonour) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface existingInstrumentDishonour = null;
        if (instrumentDishonour != null){
            existingInstrumentDishonour = instrumentDishonourRepository.findOne(instrumentDishonour.getId());
        }
        return existingInstrumentDishonour;
    }

    @Override
    public InstrumentDishonourInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface existingInstrumentDishonour = null;
        if(id != 0){
            existingInstrumentDishonour = instrumentDishonourRepository.findOne(id);
        }
        return existingInstrumentDishonour;
    }

    @Override
    public InstrumentDishonourInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface existingInstrumentDishonour = null;
        if(consumerNo != null){
            existingInstrumentDishonour = instrumentDishonourRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return existingInstrumentDishonour;
    }

}
