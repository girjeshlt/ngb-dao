package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.RegionDAOInterface;
import com.mppkvvcl.ngbdao.repositories.RegionRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class RegionDAO implements RegionDAOInterface<RegionInterface> {


    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(RegionDAO.class);

    @Autowired
    private RegionRepository regionRepository;
    
    @Override
    public RegionInterface add(RegionInterface region) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        RegionInterface insertedRegion = null;
        if (region != null){
            insertedRegion = regionRepository.save(region);
        }
        return insertedRegion;    }

    @Override
    public RegionInterface update(RegionInterface region) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        RegionInterface updatedRegion = null;
        if (region != null){
            updatedRegion = regionRepository.save(region);
        }
        return updatedRegion;
    }

    @Override
    public RegionInterface get(RegionInterface region) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        RegionInterface existingRegion = null;
        if (region != null){
            existingRegion = regionRepository.findOne(region.getId());
        }
        return existingRegion;
    }

    @Override
    public RegionInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        RegionInterface existingRegion = null;
        if(id != 0){
            existingRegion = regionRepository.findOne(id);
        }
        return existingRegion;
    }

    @Override
    public List<? extends RegionInterface> getAll() {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        List<? extends RegionInterface> existingRegions = regionRepository.findAll();
        return existingRegions;
    }
}
