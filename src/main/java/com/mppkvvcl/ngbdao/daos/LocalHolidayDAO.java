package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.LocalHolidayDAOInterface;
import com.mppkvvcl.ngbdao.repositories.LocalHolidayRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.LocalHolidayInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * coded by nitish on 27-09-2017
 */
@Service
public class LocalHolidayDAO implements LocalHolidayDAOInterface<LocalHolidayInterface> {

    private static final Logger logger = GlobalResources.getLogger(LocalHolidayDAO.class);

    @Autowired
    private LocalHolidayRepository localHolidayRepository;

    @Override
    public LocalHolidayInterface getByLocationCodeAndDate(String locationCode, Date date) {
        final String methodName = "getByLocationCodeAndDate() : ";
        logger.info(methodName + "called for locationCode " + locationCode +" " + date);
        LocalHolidayInterface localHoliday = null;
        if(locationCode != null && date != null){
            localHoliday = localHolidayRepository.findByLocationCodeAndDate(locationCode,date);
        }
        return localHoliday;
    }

    @Override
    public LocalHolidayInterface add(LocalHolidayInterface localHolidayInterface) {
        return null;
    }

    @Override
    public LocalHolidayInterface update(LocalHolidayInterface localHolidayInterface) {
        return null;
    }

    @Override
    public LocalHolidayInterface get(LocalHolidayInterface localHolidayInterface) {
        return null;
    }

    @Override
    public LocalHolidayInterface getById(long id) {
        return null;
    }
}
