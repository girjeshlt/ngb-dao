package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SecurityDepositInterestDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SecurityDepositInterestRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.SecurityDepositInterest;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */

@Service
public class SecurityDepositInterestDAO implements SecurityDepositInterestDAOInterface<SecurityDepositInterestInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SecurityDepositInterestDAO.class);

    @Autowired
    private SecurityDepositInterestRepository securityDepositInterestRepository;

    @Override
    public SecurityDepositInterestInterface add(SecurityDepositInterestInterface securityDepositInterestToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestInterface insertedSecurityDepositInterest = null;
        if (securityDepositInterestToAdd != null){
            insertedSecurityDepositInterest = securityDepositInterestRepository.save(securityDepositInterestToAdd);
        }
        return insertedSecurityDepositInterest;
    }

    @Override
    public List<SecurityDepositInterestInterface> add(List<SecurityDepositInterestInterface> securityDepositInterestsToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterestInterface> insertedSecurityDepositInterests = null;
        if (securityDepositInterestsToAdd != null) {
            insertedSecurityDepositInterests = new ArrayList<>();
            for (SecurityDepositInterestInterface securityDepositInterestInterface : securityDepositInterestsToAdd) {
                SecurityDepositInterestInterface insertedSecurityDeposit = add(securityDepositInterestInterface);
                insertedSecurityDepositInterests.add(insertedSecurityDeposit);
            }
        }
        return insertedSecurityDepositInterests;
    }

    @Override
    public SecurityDepositInterestInterface update(SecurityDepositInterestInterface securityDepositInterestToUpdated) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestInterface updatedSecurityDepositInterest = null;
        if (securityDepositInterestToUpdated != null){
            updatedSecurityDepositInterest = securityDepositInterestRepository.save(securityDepositInterestToUpdated);
        }
        return updatedSecurityDepositInterest;
    }

    @Override
    public SecurityDepositInterestInterface get(SecurityDepositInterestInterface securityDepositInterest) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestInterface existingSecurityDepositInterest = null;
        if (securityDepositInterest != null){
            existingSecurityDepositInterest = securityDepositInterestRepository.findOne(securityDepositInterest.getId());
        }
        return existingSecurityDepositInterest;
    }

    @Override
    public SecurityDepositInterestInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestInterface existingSecurityDepositInterest = null;
        existingSecurityDepositInterest = securityDepositInterestRepository.findOne(id);
        return existingSecurityDepositInterest;
    }

    @Override
    public List<SecurityDepositInterestInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterestInterface> fetchedSecurityDepositInterest = null;
        if(consumerNo != null){
            fetchedSecurityDepositInterest = securityDepositInterestRepository.findByConsumerNo(consumerNo);
        }
        return fetchedSecurityDepositInterest;
    }

    @Override
    public SecurityDepositInterestInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestInterface fetchedSecurityDepositInterest = null;
        if(consumerNo != null){
            fetchedSecurityDepositInterest = securityDepositInterestRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return fetchedSecurityDepositInterest;
    }

    @Override
    public List<SecurityDepositInterestInterface> getTop2ByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTop2ByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterestInterface> securityDepositInterestInterfaces = null;
        if(consumerNo != null){
            securityDepositInterestInterfaces = securityDepositInterestRepository.findTop2ByConsumerNoOrderByIdDesc(consumerNo);
        }
        return  securityDepositInterestInterfaces;
    }
}
