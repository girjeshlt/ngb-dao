package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.EmployeeConnectionMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.EmployeeConnectionMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class EmployeeConnectionMappingDAO implements EmployeeConnectionMappingDAOInterface<EmployeeConnectionMappingInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(EmployeeConnectionMappingDAO.class);

    @Autowired
    EmployeeConnectionMappingRepository employeeConnectionMappingRepository;

    @Override
    public EmployeeConnectionMappingInterface add(EmployeeConnectionMappingInterface employeeConnectionMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        EmployeeConnectionMappingInterface insertedEmployeeConnectionMapping = null;
        if (employeeConnectionMapping != null){
            insertedEmployeeConnectionMapping = employeeConnectionMappingRepository.save(employeeConnectionMapping);
        }
        return insertedEmployeeConnectionMapping;
    }

    @Override
    public EmployeeConnectionMappingInterface update(EmployeeConnectionMappingInterface employeeConnectionMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        EmployeeConnectionMappingInterface updatedEmployeeConnectionMapping = null;
        if (employeeConnectionMapping != null){
            updatedEmployeeConnectionMapping = employeeConnectionMappingRepository.save(employeeConnectionMapping);
        }
        return updatedEmployeeConnectionMapping;
    }

    @Override
    public EmployeeConnectionMappingInterface get(EmployeeConnectionMappingInterface employeeConnectionMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        EmployeeConnectionMappingInterface existingEmployeeConnectionMapping = null;
        if (employeeConnectionMapping != null){
            existingEmployeeConnectionMapping = employeeConnectionMappingRepository.findOne(employeeConnectionMapping.getId());
        }
        return existingEmployeeConnectionMapping;
    }

    @Override
    public EmployeeConnectionMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        EmployeeConnectionMappingInterface existingEmployeeConnectionMapping = null;
        if(id != 0){
            existingEmployeeConnectionMapping = employeeConnectionMappingRepository.findOne(id);
        }
        return existingEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByEmployeeNo(String employeeNo) {
        final String methodName = "getByEmployeeNo() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(employeeNo != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByEmployeeNo(employeeNo);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(consumerNo != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByConsumerNo(consumerNo);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByStatus(String status) {
        final String methodName = "getByStatus() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(status != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByStatus(status);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByEmployeeNoAndStatus(String employeeNo, String status) {
        final String methodName = "getByEmployeeNoAndStatus() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(employeeNo != null && status != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByEmployeeNoAndStatus(employeeNo , status);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByConsumerNoAndStatus(String consumerNo, String status) {
        final String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(consumerNo != null && status != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByConsumerNoAndStatus(consumerNo , status);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByEmployeeNoAndConsumerNoAndStatus(String employeeNo, String consumerNo, String status) {
        final String methodName = "getByEmployeeNoAndConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(employeeNo != null && consumerNo != null && status != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByEmployeeNoAndConsumerNoAndStatus(employeeNo , consumerNo , status);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByStartDateBetween(Date startDate, Date endDate) {
        final String methodName = "getByStartDateBetween() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(startDate != null && endDate != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByStartDateBetween(startDate , endDate);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByEndDateBetween(Date startDate, Date endDate) {
        final String methodName = "getByEndDateBetween() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(startDate != null && endDate != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByEndDateBetween(startDate , endDate);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByStartBillMonthBetween(String startBillMonth, String endBillMonth) {
        final String methodName = "getByStartBillMonthBetween() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(startBillMonth != null && endBillMonth != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByStartBillMonthBetween(startBillMonth , endBillMonth);
        }
        return fetchedEmployeeConnectionMapping;
    }

    @Override
    public List<EmployeeConnectionMappingInterface> getByEndBillMonthBetween(String startBillMonth, String endBillMonth) {
        final String methodName = "getByEndBillMonthBetween() : ";
        logger.info(methodName + "called");
        List<EmployeeConnectionMappingInterface> fetchedEmployeeConnectionMapping = null;
        if(startBillMonth != null && endBillMonth != null){
            fetchedEmployeeConnectionMapping = employeeConnectionMappingRepository.findByEndBillMonthBetween(startBillMonth , endBillMonth);
        }
        return fetchedEmployeeConnectionMapping;
    }
}
