package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterMasterDAO implements MeterMasterDAOInterface<MeterMasterInterface> {

    Logger logger = GlobalResources.getLogger(MeterMasterDAO.class);

    @Autowired
    MeterMasterRepository meterMasterRepository;

    @Override
    public MeterMasterInterface add(MeterMasterInterface meterMaster) {
        final String methodName = "add() : ";
        logger.info(methodName+"clicked");
        MeterMasterInterface insertedMeterMaster = null;
        if (meterMaster != null){
            insertedMeterMaster = meterMasterRepository.save(meterMaster);
        }
        return insertedMeterMaster;
    }

    @Override
    public MeterMasterInterface update(MeterMasterInterface meterMaster) {
        final String methodName = "update() : ";
        logger.info(methodName+"clicked");
        MeterMasterInterface updateMeterMaster = null;
        if (meterMaster != null){
            updateMeterMaster = meterMasterRepository.save(meterMaster);
        }
        return updateMeterMaster;
    }

    @Override
    public MeterMasterInterface get(MeterMasterInterface meterMaster) {
        final String methodName = "get() : ";
        logger.info(methodName+"clicked");
        MeterMasterInterface getMeterMaster = null;
        if (meterMaster != null){
            getMeterMaster = meterMasterRepository.findOne(meterMaster.getId());
        }
        return getMeterMaster;
    }

    @Override
    public MeterMasterInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName+"clicked");
        MeterMasterInterface meterMaster = null;
        if (id > 0){
            meterMaster = meterMasterRepository.findOne(id);
        }
        return meterMaster;
    }

    @Override
    public MeterMasterInterface getByIdentifier(String identifier) {
        final String methodName = "getByIdentifier()  : ";
        logger.info(methodName+"clicked");
        MeterMasterInterface meterMaster =  null;
        if (identifier != null){
            meterMaster = meterMasterRepository.findByIdentifier(identifier);
        }
        return meterMaster;
    }

    @Override
    public List<MeterMasterInterface> getBySerialNo(String serialNo) {
        final String methodName = "getBySerialNo() : ";
        logger.info(methodName+"clicked");
        List<MeterMasterInterface> meterMasters = null;
        if (serialNo != null){
            meterMasters = meterMasterRepository.findBySerialNo(serialNo);
        }
        return meterMasters;
    }
}
