package com.mppkvvcl.ngbdao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class NgbDaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NgbDaoApplication.class, args);
	}
}
