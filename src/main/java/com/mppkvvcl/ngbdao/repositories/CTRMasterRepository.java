package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbentity.beans.CTRMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vikas Patel on 6/21/2017.
 */
@Repository
public interface CTRMasterRepository extends JpaRepository<CTRMaster, Long>{
    /**
     * To find CTR Details by giving Serial Number
     * param serialNo
     * return List of CTRMasterRepository
     */
     public List<CTRMasterInterface> findBySerialNo(String serialNo);

    /**
     * To find CTR Details by giving make
     * param make
     * return List of CTRMaster BySerialNo(String serialNo);
     */
     public List<CTRMasterInterface> findByMake(String make);

    /**
     * To find CTR Details by giving Identifier
     * param identifier
     * return CTRMaster
     */
    public CTRMasterInterface findByIdentifier(String identifier);

    /**
     * To find CTRMaster by providing Serial No and Make
     * param serialNo
     * param make
     * return CTRMaster
     */
    public CTRMasterInterface findBySerialNoAndMake(String serialNo,String make);

    public CTRMasterInterface save(CTRMasterInterface ctrMaster);
}
