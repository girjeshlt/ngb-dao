package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;
import com.mppkvvcl.ngbentity.beans.AgricultureBill6Monthly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgricultureBill6MonthlyRepository extends JpaRepository<AgricultureBill6Monthly,Long>{
    public AgricultureBill6MonthlyInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);

    public AgricultureBill6MonthlyInterface save(AgricultureBill6MonthlyInterface agricultureBill6MonthlyInterface);
}
