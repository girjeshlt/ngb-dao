package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConnectionTypeInterface;
import com.mppkvvcl.ngbentity.beans.ConnectionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ANSHIKA on 18-07-2017.
 */
@Repository
public interface ConnectionTypeRepository extends JpaRepository<ConnectionType, Long>{
    public ConnectionTypeInterface save(ConnectionTypeInterface connectionTypeInterface);
}
