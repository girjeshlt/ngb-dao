package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;
import com.mppkvvcl.ngbentity.beans.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ANSHIKA on 17-07-2017.
 */
@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {

    public ZoneInterface findByCode(String code);

    public ZoneInterface save(ZoneInterface zoneInterface);

    public List<ZoneInterface> findByDivisionId(long divisionId);
}
