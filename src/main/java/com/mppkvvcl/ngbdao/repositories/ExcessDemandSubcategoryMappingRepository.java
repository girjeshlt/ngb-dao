package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.ExcessDemandSubcategoryMapping;
import com.mppkvvcl.ngbinterface.interfaces.ExcessDemandSubcategoryMappingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcessDemandSubcategoryMappingRepository extends JpaRepository<ExcessDemandSubcategoryMapping,Long>{
    public List<ExcessDemandSubcategoryMappingInterface> findByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);
    public ExcessDemandSubcategoryMappingInterface save(ExcessDemandSubcategoryMappingInterface excessDemandSubcategoryMappingInterface);
}
