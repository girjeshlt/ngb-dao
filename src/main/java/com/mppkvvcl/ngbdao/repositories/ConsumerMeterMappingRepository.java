package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerMeterMappingInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerMeterMapping;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

/**
 * Created by Vikas Patel on 6/21/2017.
 *
 */
@Repository
public interface ConsumerMeterMappingRepository extends JpaRepository <ConsumerMeterMapping, Long >{

    /**
     * to get details of consumer meter  by Consumer number
     * param consumerNo type String
     * return List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByConsumerNo(String consumerNo);

    /**
     * ConsumerMeterMappingInterfaceo get details of Consumer meter mapping by Meter Identifier
     * param meterIdentifier String
     * return List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByMeterIdentifier(String meterIdentifier);

    /**
     * to get details of consumer meter making  by Meter Identifier and Mapping Status
     * param meterIdentifier and mappingStatus  String
     * return List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByMeterIdentifierAndMappingStatus(String meterIdentifier,String mappingStatus);

    /**
     * to get Consumer Mapping meter details by giving Consumer no and Meter Identifier
     * param consumerNo and meterIdentifier String
     * return ConsumerMeterMapping
      */
    public ConsumerMeterMappingInterface findByConsumerNoAndMeterIdentifier(String consumerNo, String meterIdentifier);

    /**
     * to get Consumer Mapping meter details by giving Consumer no and mapping status and Meter Identifier
     * param consumerNo and mappingStatus String
     * return ConsumerMeterMapping
     */
    public ConsumerMeterMappingInterface findByConsumerNoAndMeterIdentifierAndMappingStatus(String consumerNo, String meterIdentifier, String mappingStatus);

    /**
     *  to get consumer meter mapping by meter Serial number
     * param meterSerialNo ConsumerMeterMappingInterfaceype String
     * return  List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByMeterSerialNo(String meterSerialNo);

    /**
     * to get details of  meter by meter make
     * param meterMake String
     * return  List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByMeterMake(String meterMake);

    /**
     * to get details of Consumer meters by consumer no and Mappin status
     * param consumerNo type String
     * param mappingStatus type String
     * return   List of ConsumerMeterMapping
     */
    public  List<ConsumerMeterMappingInterface> findByConsumerNoAndMappingStatus(String consumerNo,String mappingStatus);

    /**
     * to get details of meters  by given start date period
     * param startDate type  Date
     * param endDate    type date
     * return List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByInstallationDateBetween(Date startDate,Date endDate);

    /**
     * to get details of meters by given  removal date period
     * param startDate  type  Date
     * param endDate type  Date
     * return List of ConsumerMeterMapping
     */
    public List<ConsumerMeterMappingInterface> findByRemovalDateBetween(Date startDate,Date endDate);



    /**
     * to get details of meter by given start/end install Bill month
     * param installationBillMonthStart String
     * param installationBillMonthEnd   String
     * return List of ConsumerMeterMapping
     */
    @Query("from ConsumerMeterMapping cmm where to_date(cmm.installationBillMonth,'MON-YYYY') between to_date(:month1,'MON-YYYY') and to_date(:month2,'MON-YYYY')")
    public  List<ConsumerMeterMappingInterface> findByInstallationBillMonth(@Param("month1") String installationBillMonthStart, @Param("month2") String installationBillMonthEnd);

    /**
     *to get details of meter by given start/end install Bill month and consumer no
     * param consumerNo String
     * param installationBillMonthStart String
     * param installationBillMonthEnd   String
     * return
     */
    @Query("from ConsumerMeterMapping cmm where consumerNo = :consumerNo and to_date(cmm.installationBillMonth,'MON-YYYY') between to_date(:month1,'MON-YYYY') and to_date(:month2,'MON-YYYY')")
    public  List<ConsumerMeterMappingInterface> findByConsumerNoAndInstallationBillMonth(@Param("consumerNo") String consumerNo, @Param("month1") String installationBillMonthStart, @Param("month2") String installationBillMonthEnd);

    /**
     *  to get details of meter by given start/end removal Bill month
     *  param removalBillMonthStart   String
     *  param removalBillMonthEnd String
     *  return   List of ConsumerMeterMapping
     */
    @Query("from ConsumerMeterMapping cmm where to_date(cmm.removalBillMonth,'MON-YYYY') between to_date(:month1,'MON-YYYY') and to_date(:month2,'MON-YYYY')")
    public  List<ConsumerMeterMappingInterface> findByRemovalBillMonth(@Param("month1") String removalBillMonthStart, @Param("month2") String removalBillMonthEnd);

    /**
     *to get details of meter by given start/end removal Bill month and consumer no
     * param consumerNo String
     * param removalBillMonthStart String
     * param removalBillMonthEnd   String
     * return
     */
    @Query("from ConsumerMeterMapping cmm where consumerNo = :consumerNo and to_date(cmm.removalBillMonth,'MON-YYYY') between to_date(:month1,'MON-YYYY') and to_date(:month2,'MON-YYYY')")
    public  List<ConsumerMeterMappingInterface> findByConsumerNoAndRemovalBillMonth(@Param("consumerNo") String consumerNo, @Param("month1") String removalBillMonthStart, @Param("month2") String removalBillMonthEnd);


    public ConsumerMeterMappingInterface save(ConsumerMeterMappingInterface consumerMeterMappingInterface);

    //count  methods
    public long countByConsumerNo(String consumerNo);

    public long countByMeterIdentifier(String meterIdentifier);
    
    //pageable methods
    public List<ConsumerMeterMappingInterface> findByConsumerNo(String consumerNo, Pageable pageable);

    @Query("from #{#entityName} cmmi where cmmi.consumerNo= :consumerNo order by to_date(cmmi.installationBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<ConsumerMeterMappingInterface> findByConsumerNoOrderByInstallationBillMonthDescending(@Param("consumerNo")String consumerNo, Pageable pageable);

    @Query("from #{#entityName} cmmi where cmmi.consumerNo= :consumerNo order by to_date(cmmi.installationBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<ConsumerMeterMappingInterface> findByConsumerNoOrderByInstallationBillMonthAscending(@Param("consumerNo")String consumerNo, Pageable pageable);

    public List<ConsumerMeterMappingInterface> findByMeterIdentifier(String meterIdentifier,Pageable pageable);

    @Query("from #{#entityName} cmmi where cmmi.meterIdentifier= :meterIdentifier order by to_date(cmmi.installationBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<ConsumerMeterMappingInterface> findByMeterIdentifierOrderByInstallationBillMonthDescending(@Param("meterIdentifier")String meterIdentifier,Pageable pageable);

    @Query("from #{#entityName} cmmi where cmmi.meterIdentifier= :meterIdentifier order by to_date(cmmi.installationBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<ConsumerMeterMappingInterface> findByMeterIdentifierOrderByInstallationBillMonthAscending(@Param("meterIdentifier")String meterIdentifier,Pageable pageable);
}

