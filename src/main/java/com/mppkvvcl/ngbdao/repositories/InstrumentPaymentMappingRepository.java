package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import com.mppkvvcl.ngbentity.beans.InstrumentPaymentMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by RUPALI on 14-07-2017.
 */
@Repository
public interface InstrumentPaymentMappingRepository extends JpaRepository<InstrumentPaymentMapping, Long>{

    public List<InstrumentPaymentMappingInterface> findByInstrumentDetailId(long instrumentDetailId);
    public InstrumentPaymentMappingInterface findByPaymentId(long paymentId);
    public List<InstrumentPaymentMappingInterface> findByInstrumentDetailIdOrderByPaymentIdAsc(long instrumentDetailId);
    public InstrumentPaymentMappingInterface save(InstrumentPaymentMappingInterface instrumentPaymentMappingInterface);
    public void delete(InstrumentPaymentMappingInterface instrumentPaymentMappingInterface);
}
