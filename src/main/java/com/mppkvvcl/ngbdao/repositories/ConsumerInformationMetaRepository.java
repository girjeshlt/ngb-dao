package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationMetaInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerInformationMeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/7/2017.
 */
@Repository
public interface ConsumerInformationMetaRepository extends JpaRepository<ConsumerInformationMeta,Long> {
        public ConsumerInformationMetaInterface save(ConsumerInformationMetaInterface consumerInformationMeta);
}

