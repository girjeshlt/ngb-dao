package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.FCAInterface;
import com.mppkvvcl.ngbentity.beans.FCA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface FCARepository extends JpaRepository<FCA,Long>{
    @Query("from FCA f where :date >= f.effectiveStartDate and :date <= f.effectiveEndDate")
    public FCAInterface findByDate(@Param("date") Date date);

    public FCAInterface save(FCAInterface fcaInterface);

    @Query("from FCA f where (:startDate >= f.effectiveStartDate and :startDate <= f.effectiveEndDate) or (:endDate >= f.effectiveStartDate and :endDate <= f.effectiveEndDate)")
    public List<FCAInterface> findByEffectiveStartDateAndEffectiveEndDate(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
}
