package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationHistoryInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerInformationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/7/2017.
 */
@Repository
public interface ConsumerInformationHistoryRepository extends JpaRepository<ConsumerInformationHistory,Long> {


    public List<ConsumerInformationHistoryInterface> findByConsumerNo(String consumerNo);
    public List<ConsumerInformationHistoryInterface> findByEndDateBetween(Date startingDate, Date endingDate);
    public List<ConsumerInformationHistoryInterface> findByPropertyNameAndEndDateBetween(String propertyName, Date startingDate, Date endingDate);

    public ConsumerInformationHistoryInterface save(ConsumerInformationHistoryInterface consumerInformationHistory);
}
