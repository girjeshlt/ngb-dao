package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.EmployeeMasterInterface;
import com.mppkvvcl.ngbentity.beans.EmployeeMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface EmployeeMasterRepository extends JpaRepository<EmployeeMaster,Long> {

    public EmployeeMasterInterface findByEmployeeNo(String employeeNo);

    public List<EmployeeMasterInterface> findByCompany(String company);

    public EmployeeMasterInterface findByEmployeeNoAndStatus(String employeeNo, String status);

    public EmployeeMasterInterface findByEmployeeNoAndTypeAndStatus(String employeeNo, String type, String status);

    public EmployeeMasterInterface save(EmployeeMasterInterface employeeMasterInterface);
}
