package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.Payment;
import com.mppkvvcl.ngbentity.beans.PaymentStaging;
import com.mppkvvcl.ngbinterface.interfaces.PaymentStagingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 1/2/2018.
 */
@Repository
public interface PaymentStagingRepository extends JpaRepository<PaymentStaging, Long> {

    public List<PaymentStagingInterface> findByIvrs(String ivrs);

    public List<PaymentStagingInterface> findByServiceNo(String serviceNo);

    public PaymentStagingInterface findByTransactionId(String transactionId);

    public PaymentStagingInterface save(PaymentStagingInterface paymentStagingInterface);

    public List<PaymentStagingInterface> findByDateOfTransaction(Date dateOfTransaction);

    public List<PaymentStagingInterface> findByStatus(String status);

    public List<PaymentStagingInterface> findByLocationCode(String locationCode);

    public List<PaymentStagingInterface> findByLocationCodeAndStatus(String locationCode, String status);

    public List<PaymentStagingInterface> findByLocationCodeAndDateOfTransactionAndStatus(String locationCode,Date dateOfTransaction, String status);

    public List<PaymentStagingInterface> findByDateOfTransactionAndStatus(Date dateOfTransaction, String status);

    public List<PaymentStagingInterface> findByIvrsAndStatus(String ivrs, String status);

}
