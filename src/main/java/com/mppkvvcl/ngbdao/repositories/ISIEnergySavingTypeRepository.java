package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;
import com.mppkvvcl.ngbentity.beans.ISIEnergySavingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ISIEnergySavingTypeRepository extends JpaRepository<ISIEnergySavingType,Long> {

    public List<ISIEnergySavingTypeInterface> findByType(String type);
    public ISIEnergySavingTypeInterface save(ISIEnergySavingTypeInterface isiEnergySavingTypeInterface);
}
