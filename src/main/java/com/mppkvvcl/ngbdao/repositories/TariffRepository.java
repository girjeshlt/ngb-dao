package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import com.mppkvvcl.ngbentity.beans.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Girjesh Kumar Suryawanshi on 5/19/2017.
 * This is TariffRepository Interface  contain astract method which is used to perform various
 * Tariff table related operation
 *
 * UPDATED BY : SUMIT VERMA on 03/06/2017
 * UPDATION : Added new method findByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate
 * with tariffType as input parameter which is another filter in process of finding tariffs.
 * Repos
 */
@Repository
public interface TariffRepository extends JpaRepository<Tariff, Long> {

    /**
     * This method take tariffCategory and date as argument and return
     * those tariff whose date fall between starteffectivedate and endeffectivefdate
     **/
    @Query("from tariff t where t.tariffCategory = :tariffCategory and to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:date,'DD MM YYYY')" +
            " and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:date,'DD MM YYYY')")
    public List<TariffInterface> findByTariffCategoryAndEffectiveDate(@Param("tariffCategory") String tariffCategory, @Param("date") String date);

    /**
     * This method three argument  tariffCategory  meteringstatus and date as argument and return
     * those tariff whose date fall between starteffectivedate and endeffectivefdate
     */
    @Query("from tariff t where t.tariffCategory = :tariffCategory and t.meteringStatus = :meteringStatus and to_date(t.effectiveStartDate ,'DD MM YYYY')" +
            " <= to_date(:date,'DD MM YYYY') and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:date,'DD MM YYYY')")
    public List<TariffInterface> findByTariffCategoryAndMeteringStatusAndEffectiveDate(@Param("tariffCategory") String tariffCategory, @Param("meteringStatus") String meteringStatus, @Param("date") String date);

    /**
     * This method take three argument and connectiuontype  and date as argument and return
     * those tariff whose date fall between starteffectivedate and endeffectivefdate
     */
    @Query("from tariff t where t.tariffCategory = :tariffCategory and t.connectionType = :connectionType and to_date(t.effectiveStartDate ,'DD MM YYYY') " +
            "<= to_date(:date,'DD MM YYYY') and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:date,'DD MM YYYY')")
    public List<TariffInterface> findByTariffCategoryAndConnectionTypeAndEffectiveDate(@Param("tariffCategory") String tariffCategory, @Param("connectionType") String connectionType, @Param("date") String date);

    /* This method take four argument tariffCategory,meteringstatus ,connectiontype  and date as argument and return  those tariff whose date fall between starteffectivedate and endeffectivefdate    */
    @Query("from tariff t where t.tariffCategory = :tariffCategory and t.meteringStatus = :meteringStatus and t.connectionType = :connectionType and " +
            "to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:date,'DD MM YYYY') and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:date,'DD MM YYYY')")
    public List<TariffInterface> findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(@Param("tariffCategory") String tariffCategory, @Param("meteringStatus") String meteringStatus, @Param("connectionType") String connectionType, @Param("date") String date);

    /* This method take four argument tariffCategory,meteringstatus ,connectiontype,tariffType  and date as argument and return  those tariffs whose date fall between starteffectivedate and endeffectivefdate    */
    @Query("from tariff t where t.tariffCategory = :tariffCategory and t.meteringStatus = :meteringStatus and t.connectionType = :connectionType and " +
            "t.tariffType = :tariffType and to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:date,'DD MM YYYY') and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:date,'DD MM YYYY')")
    public List<TariffInterface> findByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(
            @Param("tariffCategory") String tariffCategory, @Param("meteringStatus") String meteringStatus
            , @Param("connectionType") String connectionType, @Param("tariffType") String tariffType,@Param("date") String date);



    /* This method take only one argument id and return single row      */
    public TariffInterface findById(long id);

    @Query("from tariff t where t.tariffCode = :tariffCode and to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:effectiveDate,'DD MM YYYY') and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:effectiveDate,'DD MM YYYY')")
    public TariffInterface findByTariffCodeAndEffectiveDate(@Param("tariffCode") String tariffCode, @Param("effectiveDate") String effectiveDate);

    public TariffInterface findTopByTariffCodeOrderByIdDesc(String tariffCode);

    public TariffInterface save(TariffInterface tariffInterface);

    @Query("from tariff t where t.tariffCode = :tariffCode and (to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:effectiveStartDate,'DD MM YYYY') " +
            "and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:effectiveStartDate,'DD MM YYYY') or to_date(t.effectiveStartDate ,'DD MM YYYY') <= to_date(:effectiveEndDate,'DD MM YYYY') " +
            "and to_date(t.effectiveEndDate ,'DD MM YYYY') > to_date(:effectiveEndDate,'DD MM YYYY')) order by t.id desc")
    public List<TariffInterface> findByTariffCodeAndEffectiveStartDateAndEffectiveEndDate(@Param("tariffCode") String tariffCode, @Param("effectiveStartDate") String effectiveStartDate,@Param("effectiveEndDate") String effectiveEndDate);

}
