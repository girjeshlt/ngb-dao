package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.SecurityDepositInterest;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestInterface;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Repository
public interface SecurityDepositInterestRepository extends JpaRepository<SecurityDepositInterest, Long> {

    public SecurityDepositInterestInterface save(SecurityDepositInterestInterface securityDepositInterestInterface);
    public List<SecurityDepositInterestInterface> findByConsumerNo(String consumerNo);
    public SecurityDepositInterestInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);
    public List<SecurityDepositInterestInterface> findTop2ByConsumerNoOrderByIdDesc(String consumerNo);

}
