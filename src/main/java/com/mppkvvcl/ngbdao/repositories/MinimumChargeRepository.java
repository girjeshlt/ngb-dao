package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.MinimumCharge;
import com.mppkvvcl.ngbinterface.interfaces.MinimumChargeInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinimumChargeRepository extends JpaRepository<MinimumCharge,Long>{
    public MinimumChargeInterface findByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode);
}
