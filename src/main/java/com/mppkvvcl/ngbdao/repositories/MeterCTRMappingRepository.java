package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import com.mppkvvcl.ngbentity.beans.MeterCTRMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 7/4/2017.
 */
@Repository
public interface MeterCTRMappingRepository extends JpaRepository<MeterCTRMapping, Long> {

    public List<MeterCTRMappingInterface> findByMeterIdentifier(String meterIdentifier);

    public List<MeterCTRMappingInterface> findByMeterIdentifierAndMappingStatus(String meterIdentifier, String mappingStatus);
    public List<MeterCTRMappingInterface> findByCtrIdentifierAndMappingStatus(String ctrIdentifier, String mappingStatus);

    public MeterCTRMappingInterface save(MeterCTRMappingInterface meterCTRMappingInterface);
}


