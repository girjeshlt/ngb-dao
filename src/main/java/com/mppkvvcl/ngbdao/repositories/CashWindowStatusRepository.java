package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import com.mppkvvcl.ngbentity.beans.CashWindowStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Repository
public interface CashWindowStatusRepository extends JpaRepository<CashWindowStatus, Long>{


    List<CashWindowStatusInterface> findByUsernameAndStatus(String username, String status);

    List<CashWindowStatusInterface> findByLocationCodeAndDateAndStatus(String location, Date date, String status);

    CashWindowStatusInterface findByLocationCodeAndWindowNameAndDate(String locationCode, String windowName, Date freezeOnDate);

    List<CashWindowStatusInterface> findByUsernameAndStatusAndDate(String username, String status, Date freezeOnDate);

    List<CashWindowStatusInterface> findByLocationCodeAndStatus(String locationCode, String statusOpen);

    CashWindowStatusInterface save(CashWindowStatusInterface cashWindowStatusInterface);
}
