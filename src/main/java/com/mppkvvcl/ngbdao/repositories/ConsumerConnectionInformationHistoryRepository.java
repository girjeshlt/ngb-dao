package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationHistoryInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionInformationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ConsumerConnectionInformationHistoryRepository extends JpaRepository<ConsumerConnectionInformationHistory,Long> {

    /**
     * to get ConsumerConnectionInformationHistory by giving ConsumerNo
     * param consumerNo
     * return List of ConsumerConnectionInformationHistory
     */
    public List<ConsumerConnectionInformationHistoryInterface> findByConsumerNo(String consumerNo);

    /**
     * to get ConsumerConnectionInformationHistory by giving ConsumerNo AND PropertyName
     * param consumerNo
     * param propertyName
     * return List of ConsumerConnectionInformationHistory
     */
    public List<ConsumerConnectionInformationHistoryInterface> findByConsumerNoAndPropertyName(String consumerNo, String propertyName);


    /**
     * TO get  ConsumerConnectionInformationHistory by  giving Date Range
     * param startDate
     * param endDate
     * return
     */
    public List<ConsumerConnectionInformationHistoryInterface> findByConsumerNoAndPropertyNameAndEndDateBetween(String consumerNo,String propertyName ,Date startDate,Date endDate);

    /**
     * TO get  ConsumerConnectionInformationHistory by  giving bill month
     * param endMonth
     * return List of ConsumerConnectionInformationHistory
     */
    public List<ConsumerConnectionInformationHistoryInterface> findByConsumerNoAndEndBillMonth(String consumerNo,String endMonth);

    /**
     * TO get  ConsumerConnectionInformationHistory by  giving consumer no and property name and bill month
     * param consumerNo
     * param propertyName
     * param endMonth
     * return List of ConsumerConnectionInformationHistory
     */
    public List<ConsumerConnectionInformationHistoryInterface> findByConsumerNoAndPropertyNameAndEndBillMonth(String consumerNo,String propertyName ,String endMonth);

    public ConsumerConnectionInformationHistoryInterface save(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistory);
}
