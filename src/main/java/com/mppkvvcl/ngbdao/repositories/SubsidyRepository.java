package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.Subsidy;
import com.mppkvvcl.ngbinterface.interfaces.SubsidyInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface SubsidyRepository extends JpaRepository<Subsidy,Long>{

    public SubsidyInterface save(SubsidyInterface subsidyInterface);

    @Query("from Subsidy s where s.category = :category and s.isBpl = :isBpl and s.subcategoryCode = :subcategoryCode and :date >= s.effectiveStartDate and :date <= s.effectiveEndDate")
    public List<SubsidyInterface> findByCategoryAndIsBplAndSubcategoryCodeAndDate(@Param("category") String category, @Param("isBpl") boolean isBpl, @Param("subcategoryCode") long subcategoryCode, @Param("date") Date date);


}
