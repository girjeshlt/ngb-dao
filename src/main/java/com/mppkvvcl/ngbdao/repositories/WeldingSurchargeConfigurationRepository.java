package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.WeldingSurchargeConfiguration;
import com.mppkvvcl.ngbinterface.interfaces.WeldingSurchargeConfigurationInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeldingSurchargeConfigurationRepository extends JpaRepository<WeldingSurchargeConfiguration,Long>{

    public WeldingSurchargeConfigurationInterface findByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode);

    public WeldingSurchargeConfigurationInterface save(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface);
}
