package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionInformationInterface;
import com.mppkvvcl.ngbentity.beans.XrayConnectionInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface XrayConnectionInformationRepository extends JpaRepository<XrayConnectionInformation,Long> {

    /**
     * find xray machine data for a consumer
     * param consumerNo
     * return
     */
    public List<XrayConnectionInformationInterface> findByConsumerNo(String consumerNo);
    /**
     * find xray machine data for a consumer with status
     * param consumerNo
     * return
     */
    public List<XrayConnectionInformationInterface> findByConsumerNoAndStatus(String consumerNo, String status);

    /**
     * find consumers who are falling under specified loads.
     *
     * To find Xray consumers who's load is greater than specific load give 150 in endload and specific load in startLoad
     * param startLoad
     * param endLoad
     * return
     */
    public List<XrayConnectionInformationInterface> findByXrayLoadBetweenAndStatus(BigDecimal startLoad, BigDecimal endLoad, String status);


    /**
     * find the Xray consumers who are active/inactive in billing cycle
     * param status
     * return
     */
    public List<XrayConnectionInformationInterface> findByStatus(String status);

    /**
     * find the Xray consumers who are having Dental Xray Machines in specified range
     * To find consumers who do not have Dental Xray Machine give 0, 0 in start and end
     * param start
     * param end
     * return
     */
    public List<XrayConnectionInformationInterface> findByNoOfDentalXrayMachineBetween(Long start, Long end);

    public List<XrayConnectionInformationInterface> findByNoOfSinglePhaseXrayMachineBetween(Long start, Long end);

    public XrayConnectionInformationInterface save(XrayConnectionInformationInterface xrayConnectionInformationInterface);
}
