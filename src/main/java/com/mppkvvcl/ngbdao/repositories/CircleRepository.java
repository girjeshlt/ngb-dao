package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import com.mppkvvcl.ngbentity.beans.Circle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MITHLESH on 18-07-2017.
 */
@Repository
public interface CircleRepository extends JpaRepository<Circle, Long>{
    CircleInterface save(CircleInterface circleInterface);
}
