package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerNoMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Repository
public interface ConsumerNoMasterRepository extends JpaRepository<ConsumerNoMaster,Long>{
    public ConsumerNoMasterInterface findByOldServiceNoOne(String oldServiceNoOne);
    public ConsumerNoMasterInterface findByOldServiceNoTwo(String oldServiceNoTwo);
    public ConsumerNoMasterInterface findByConsumerNo(String consumerNo);
    public ConsumerNoMasterInterface save(ConsumerNoMasterInterface consumerNoMasterInterface);

    public long countByGroupNo(String groupNo);

    public long countByGroupNoAndStatus(String groupNo,String status);

    public long countByGroupNoAndReadingDiaryNo(String groupNo,String readingDiaryNo);

    public long countByGroupNoAndReadingDiaryNoAndStatus(String groupNo, String readingDiaryNo, String status);

    public long countByLocationCode(String locationCode);

    public long countByLocationCodeAndStatus(String locationCode,String status);

    public ConsumerNoMasterInterface findByOldServiceNoOneAndLocationCode(String oldServiceNoOne, String locationCode);
}
