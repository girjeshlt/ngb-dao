package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationMetaInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionInformationMeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ConsumerConnectionInformationMetaRepository extends JpaRepository<ConsumerConnectionInformationMeta,Long> {
    public ConsumerConnectionInformationMetaInterface save(ConsumerConnectionInformationMetaInterface consumerConnectionInformationMeta);
}
