package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.TariffDescriptionInterface;
import com.mppkvvcl.ngbentity.beans.TariffDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Girjesh Kumar Suryawanshi on 5/19/2017.
 * This is TariffDescriptionRepository Interface  contain astract method which is used to perform various
 * TariffDescription table related operation
 */
@Repository
public interface TariffDescriptionRepository extends JpaRepository<TariffDescription, Long> {
    public TariffDescriptionInterface findByTariffCategory(String tariffCategory);
    public TariffDescriptionInterface save(TariffDescriptionInterface tariffDescriptionInterface);
}
