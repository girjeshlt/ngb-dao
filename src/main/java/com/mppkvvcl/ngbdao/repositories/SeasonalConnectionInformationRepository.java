package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.SeasonalConnectionInformationInterface;
import com.mppkvvcl.ngbentity.beans.SeasonalConnectionInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface SeasonalConnectionInformationRepository extends JpaRepository<SeasonalConnectionInformation,Long> {

    /**
     * find data of seasonal consumer
     * param consumerNo
     * return
     */
    public List<SeasonalConnectionInformationInterface> findByConsumerNo(String consumerNo);


    /**
     * to find list of temporary consumers on given date
     * param givenDate
     * return
     */
    @Query("from SeasonalConnectionInformation s where :givenDate between s.seasonStartDate and s.seasonEndDate")
    public List<SeasonalConnectionInformationInterface> findBySeasonStartDateAndSeasonEndDate(@Param("givenDate") Date givenDate);

    @Query("from SeasonalConnectionInformation s where :givenDate between s.seasonStartDate and s.seasonEndDate and s.consumerNo = :consumerNo ")
    public List<SeasonalConnectionInformationInterface> findBetweenSeasonStartDateAndSeasonEndDateAndConsumerNo(@Param("givenDate") Date givenDate, @Param("consumerNo") String consumerNo);

    public List<SeasonalConnectionInformationInterface> findByViolationMonth(String violationMonth);

    public SeasonalConnectionInformationInterface save(SeasonalConnectionInformationInterface seasonalConnectionInformationInterface);

    public SeasonalConnectionInformationInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);

}
