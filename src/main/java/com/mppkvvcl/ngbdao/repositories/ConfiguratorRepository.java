package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConfiguratorInterface;
import com.mppkvvcl.ngbentity.beans.Configurator;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by PREETESH on 8/22/2017.
 */
public interface ConfiguratorRepository extends JpaRepository<Configurator,Long> {

    public ConfiguratorInterface findByCode(String code);

    public ConfiguratorInterface save(ConfiguratorInterface configuratorInterface);
}