package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.UnmeteredUnit;
import com.mppkvvcl.ngbinterface.interfaces.UnmeteredUnitInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

@Repository
public interface UnmeteredUnitRepository extends JpaRepository<UnmeteredUnit,Long> {
    public UnmeteredUnitInterface findByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode);

    public UnmeteredUnitInterface save(UnmeteredUnitInterface unmeteredUnitInterface);
}
