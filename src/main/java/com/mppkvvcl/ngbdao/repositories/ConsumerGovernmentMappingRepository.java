package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.ConsumerGovernmentMapping;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerGovernmentMappingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ConsumerGovernmentMappingRepository extends JpaRepository<ConsumerGovernmentMapping,Long> {
    public ConsumerGovernmentMappingInterface save(ConsumerGovernmentMappingInterface consumerGovernmentMapping);

    public ConsumerGovernmentMappingInterface findByConsumerNo(String consumerNo);
}
