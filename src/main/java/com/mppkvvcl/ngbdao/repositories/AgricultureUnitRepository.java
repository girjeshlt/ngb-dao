package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureUnitInterface;
import com.mppkvvcl.ngbentity.beans.AgricultureUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by MITHLESH on 14-07-2017.
 */
public interface AgricultureUnitRepository extends JpaRepository<AgricultureUnit, Long>{

    @Query("from AgricultureUnit au where to_date(:billMonth,'MON-YYYY') >= to_date(au.startMonth,'MON-YYYY') and to_date(:billMonth,'MON-YYYY') <= to_date(au.endMonth,'MON-YYYY') and au.subcategoryCode = :subcategoryCode")
    public AgricultureUnitInterface findBySubcategoryCodeAndBillMonth(@Param("subcategoryCode") long subcategoryCode, @Param("billMonth") String billMonth);

    public AgricultureUnitInterface save(AgricultureUnitInterface agricultureUnit);
}
