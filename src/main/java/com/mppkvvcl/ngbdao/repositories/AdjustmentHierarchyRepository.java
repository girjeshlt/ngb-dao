package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentHierarchyInterface;
import com.mppkvvcl.ngbentity.beans.AdjustmentHierarchy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ANKIT on 19-08-2017.
 */
@Repository
public interface AdjustmentHierarchyRepository extends JpaRepository<AdjustmentHierarchy, Long>{

    @Query("from AdjustmentHierarchy where priority >= :userPriority and priority <= :adjustmentPriority order by priority asc")
    public List<AdjustmentHierarchyInterface> findByPriorityBetweenUserPriorityAndAdjustmentPriority(@Param("userPriority") int userPriority, @Param("adjustmentPriority") int adjustmentPriority);

    public AdjustmentHierarchyInterface save(AdjustmentHierarchyInterface adjustmentHierarchyInterface);

}
