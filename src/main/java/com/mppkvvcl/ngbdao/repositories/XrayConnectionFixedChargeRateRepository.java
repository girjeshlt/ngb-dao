package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.XrayConnectionFixedChargeRate;
import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionFixedChargeRateInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 12/18/2017.
 */
@Repository
public interface XrayConnectionFixedChargeRateRepository extends JpaRepository<XrayConnectionFixedChargeRate,Long> {
    public XrayConnectionFixedChargeRateInterface save(XrayConnectionFixedChargeRateInterface xrayConnectionFixedChargeRateInterface);

    public List<XrayConnectionFixedChargeRateInterface> findByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);

    public XrayConnectionFixedChargeRateInterface findByTariffIdAndSubcategoryCodeAndXrayType(long tariffId, long subcategoryCode, String xrayType);


}
