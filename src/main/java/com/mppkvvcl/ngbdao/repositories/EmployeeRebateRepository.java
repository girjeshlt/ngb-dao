package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.EmployeeRebate;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeRebateInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface EmployeeRebateRepository extends JpaRepository<EmployeeRebate,Long>{

    @Query("from EmployeeRebate er where er.code = :code and ( :date >= er.effectiveStartDate and :date <= er.effectiveEndDate)")
    public EmployeeRebateInterface findByCodeAndDate(@Param("code") String code,@Param("date") Date date);

    public EmployeeRebateInterface save(EmployeeRebateInterface employeeRebateInterface);
}
