package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.OnlinePaymentRebate;
import com.mppkvvcl.ngbinterface.interfaces.OnlinePaymentRebateInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface OnlinePaymentRebateRepository extends JpaRepository<OnlinePaymentRebate,Long>{

    @Query("from OnlinePaymentRebate opr where :date >= opr.effectiveStartDate and :date <= opr.effectiveEndDate")
    public OnlinePaymentRebateInterface findByDate(@Param("date") Date date);

    public OnlinePaymentRebateInterface save(OnlinePaymentRebateInterface onlineRebateInterface);
}
