package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.SubCategoryInterface;
import com.mppkvvcl.ngbentity.beans.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by PREETESH on 5/19/2017.
 *
 * UPDATED BY : SUMIT VERMA on 03/06/2017
 *
 * Added new method findByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType to filter subcategories
 * by applicantType also. The applicantType provides another level of filtering to fetch subcategories.
 * The applicantType may also be used for other filtering as and when required in future.
 */
@Repository
public interface SubCategoryRepository extends JpaRepository<SubCategory, Long> {

    /**
     * this is a method to get tariff by Id
     * param tariffId
     * return
     */
    List<SubCategoryInterface> findByTariffId(long tariffId);

    /**
     * this is a method to getTariffCategory by Tariff Id and Premise Type
     * param tariffId
     * param premiseType
     * return
     */
    List<SubCategoryInterface> findByTariffIdAndPremiseType(Long tariffId, String premiseType);

    /**
     * method to find Subcategory in which given load falls
     * param tariffId
     * param connectedLoad
     * param premiseType
     * return List of SubCategory
     */
    @Query("from subcategory s where s.tariffId = :tariffId and  :connectedLoad between s.startConnectedLoadKW " +
            "and s.endConnectedLoadKW and s.premiseType = :premiseType")
    public List<SubCategoryInterface> findByTariffIdAndConnectedLoadAndPremiseType(@Param("tariffId") long tariffId, @Param ("connectedLoad") BigDecimal connectedLoad, @Param("premiseType") String premiseType);

    /**
     * to find SubCategories with tariffId ,premiseType , applicantType along with
     * in which range the passed connected load falls
     * param tariffId
     * param connectedLoad
     * param premiseType
     * param applicantType
     * return
     */
    @Query("from subcategory s where s.tariffId = :tariffId and  :connectedLoad between s.startConnectedLoadKW " +
            "and s.endConnectedLoadKW and s.premiseType = :premiseType and s.applicantType = :applicantType")
    public List<SubCategoryInterface> findByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType(@Param("tariffId") long tariffId, @Param ("connectedLoad") BigDecimal connectedLoad, @Param("premiseType") String premiseType,@Param("applicantType") String applicantType);

    /**
     * Method to find Subcategory in which given Contract Demand falls<br><br/>
     * param tariffId<br><br/>
     * param connectedLoad<br><br/>
     * param contractDemand<br><br/>
     * param premiseType<br><br/>
     * return<br><br/>
     */
    @Query("from subcategory s where s.tariffId = :tariffId and :connectedLoad between s.startConnectedLoadKW " +
            "and s.endConnectedLoadKW and  :contractDemand between s.startContractDemandKW " +
            "and s.endContractDemandKW and s.premiseType = :premiseType and s.applicantType = :applicantType")
    public List<SubCategoryInterface> findByTariffIdAndConnectedLoadAndContractDemandAndPremiseTypeAndApplicantType(@Param("tariffId") long tariffId,@Param ("connectedLoad") BigDecimal connectedLoad,@Param("contractDemand") BigDecimal contractDemand,@Param("premiseType") String premiseType,@Param("applicantType") String applicantType);

    /**
     * This method is to get latest subcategory object corresponding to given subcategory code.<br><br/>
     * This may be used under various change scenario like currently written for load change.<br><br/>
     * param code<br><br/>
     * return<br><br/>
     */
    public SubCategoryInterface findTopByCodeOrderByIdDesc(long code);

    public SubCategoryInterface save(SubCategoryInterface subCategoryInterface);

    public SubCategoryInterface findByTariffIdAndCode(long tariffId,long code);
}
