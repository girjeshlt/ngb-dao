package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbentity.beans.ReadMasterKW;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/29/2017.
 */
@Repository
public interface ReadMasterKWRepository extends JpaRepository<ReadMasterKW, Long> {

    public ReadMasterKWInterface findByReadMasterId(long readMasterId);

    public ReadMasterKWInterface save(ReadMasterKWInterface readMasterKWInterface);
}
