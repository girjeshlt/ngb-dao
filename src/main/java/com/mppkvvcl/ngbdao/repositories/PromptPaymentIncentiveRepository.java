package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.PromptPaymentIncentive;
import com.mppkvvcl.ngbinterface.interfaces.PromptPaymentIncentiveInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface PromptPaymentIncentiveRepository extends JpaRepository<PromptPaymentIncentive,Long>{

    @Query("from PromptPaymentIncentive ppi where :date >= ppi.effectiveStartDate and :date <= ppi.effectiveEndDate")
    public PromptPaymentIncentiveInterface findByDate(@Param("date") Date date);

    public PromptPaymentIncentiveInterface save(PromptPaymentIncentiveInterface promptPaymentIncentiveInterface);

}
