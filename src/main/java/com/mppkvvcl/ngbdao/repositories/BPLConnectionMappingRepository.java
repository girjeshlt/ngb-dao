package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;
import com.mppkvvcl.ngbentity.beans.BPLConnectionMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface BPLConnectionMappingRepository extends JpaRepository<BPLConnectionMapping,Long> {


    public List<BPLConnectionMappingInterface> findByConsumerNo(String consumerNo);

    /**
     * to find list of BPL consumers on given date
     * param givenDate
     * return
     */
    @Query("from BPLConnectionMapping b where :givenDate between b.startDate and b.endDate")
    public List<BPLConnectionMappingInterface> findBetweenStartDateAndEndDate(@Param("givenDate") Date givenDate);

    /**
     * find by BPL no
     * param bplNo
     * return
     */

    public List<BPLConnectionMappingInterface> findByBplNo(String bplNo);


    /**
     * find the BPL consumers who are active/inactive in billing cycle
     * param status
     * return
     */
    public List<BPLConnectionMappingInterface> findByStatus(String status);

    public BPLConnectionMappingInterface save(BPLConnectionMappingInterface bplConnectionMapping);

    public List<BPLConnectionMappingInterface> findByConsumerNoAndStatus(String consumerNo,String status);

    public List<BPLConnectionMappingInterface> findByBplNoAndStatus(String bplNo,String status);
}
