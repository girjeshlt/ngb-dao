package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import com.mppkvvcl.ngbentity.beans.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Rupali on 17-07-2017.
 */
@Repository
public interface DivisionRepository extends JpaRepository<Division, Long> {
    public List<DivisionInterface> findByCircleId(long circleId);
    public DivisionInterface save(DivisionInterface divisionInterface);
}
