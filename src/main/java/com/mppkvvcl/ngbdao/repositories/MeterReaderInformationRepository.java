package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.MeterReaderInformation;
import com.mppkvvcl.ngbinterface.interfaces.MeterReaderInformationInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ASHISH on 12/14/2017.
 */
@Repository
public interface MeterReaderInformationRepository extends JpaRepository<MeterReaderInformation, Long> {

    public MeterReaderInformationInterface save(MeterReaderInformationInterface additionalSecurityDepositInterface);

    public MeterReaderInformationInterface findByReadingDiaryNoId(long readingDiaryNoId);
}
