package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterTypeInterface;
import com.mppkvvcl.ngbentity.beans.MeterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SHIVANSHU  on 04-07-2017.
 */
@Repository
public interface MeterTypeRepository extends JpaRepository<MeterType , Long>{
    public List<MeterTypeInterface> findByMeterPhase(String meterPhase);
    public MeterTypeInterface save(MeterTypeInterface meterTypeInterface);
}
