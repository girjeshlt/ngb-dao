package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterMakeInterface;
import com.mppkvvcl.ngbentity.beans.MeterMake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */

@Repository
public interface MeterMakeRepository  extends JpaRepository<MeterMake , Long>{
    public MeterMakeInterface save(MeterMakeInterface meterMakeInterface);
}
