package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.OICZoneMappingInterface;
import com.mppkvvcl.ngbentity.beans.OICZoneMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OICZoneMappingRepository extends JpaRepository<OICZoneMapping,Long> {

    public OICZoneMappingInterface findByUserDetailUsername(String username);

    public OICZoneMappingInterface findByZoneId(long zoneId);

    public OICZoneMappingInterface save(OICZoneMappingInterface oicZoneMappingInterface);
}
