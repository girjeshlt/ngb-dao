package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.PowerFactorInterface;
import com.mppkvvcl.ngbentity.beans.PowerFactor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface PowerFactorRepository extends JpaRepository<PowerFactor,Long>{

    @Query("from PowerFactor pfr where :pfValue >= pfr.startRange and :pfValue < pfr.endRange and :date >= pfr.startDate and :date < pfr.endDate")
    public PowerFactorInterface findByPowerFactorValueAndDate(@Param("pfValue") BigDecimal pfValue, @Param("date") Date date);

    public PowerFactorInterface save(PowerFactorInterface powerFactorInterface);

    @Query("from PowerFactor pfr where :date >= pfr.startDate and :date < pfr.endDate")
    public List<PowerFactorInterface> findByDate(@Param("date") Date date);
}
