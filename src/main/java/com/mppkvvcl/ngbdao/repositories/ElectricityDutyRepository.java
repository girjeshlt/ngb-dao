package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ElectricityDutyInterface;
import com.mppkvvcl.ngbentity.beans.ElectricityDuty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 07-07-2017.
 */
@Repository
public interface ElectricityDutyRepository extends JpaRepository<ElectricityDuty, Long> {

    /**
     * to find List of ElectricityDuty by subCategoryCode and givenDate.
     * param subCategoryCode
     * param givenDate
     * return List of ElectricityDuty
     */
    @Query("from ElectricityDuty ed where ed.subCategoryCode = :subCategoryCode and :givenDate between ed.effectiveStartDate and ed.effectiveEndDate")
    public List<ElectricityDutyInterface> findBySubCategoryCodeAndDate(@Param("subCategoryCode") Long subCategoryCode, @Param("givenDate") Date givenDate);

    public ElectricityDutyInterface save(ElectricityDutyInterface electricityDuty);
}
