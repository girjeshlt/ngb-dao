package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.WindowDetailInterface;
import com.mppkvvcl.ngbentity.beans.WindowDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Repository
public interface WindowDetailRepository extends JpaRepository<WindowDetail, Long> {
    public List<WindowDetailInterface> findByLocationCode(String locationCode);
    public WindowDetailInterface save(WindowDetailInterface windowDetailInterface);
}
