package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterModemMappingInterface;
import com.mppkvvcl.ngbentity.beans.MeterModemMapping;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 21-06-2017.
 */
@Repository
public interface MeterModemMappingRepository extends JpaRepository<MeterModemMapping,Long> {

    public List<MeterModemMappingInterface> findByMeterIdentifier(String meterIdentifier);

    public List<MeterModemMappingInterface> findByMeterIdentifierAndStatus(String meterIdentifier, String status);

    public MeterModemMappingInterface save(MeterModemMappingInterface meterModemMapping);
}
