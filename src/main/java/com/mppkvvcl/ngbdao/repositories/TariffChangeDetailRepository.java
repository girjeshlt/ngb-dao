package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.TariffChangeDetailInterface;
import com.mppkvvcl.ngbentity.beans.TariffChangeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by vikas on 8/23/2017.
 */

@Repository
public interface TariffChangeDetailRepository extends JpaRepository<TariffChangeDetail,Long> {

    public TariffChangeDetailInterface findByTariffDetailId(long tariffDetailID);

    public TariffChangeDetailInterface save(TariffChangeDetailInterface tariffChangeDetailInterface);
}
