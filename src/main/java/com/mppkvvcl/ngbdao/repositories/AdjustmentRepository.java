package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */

@Repository
public interface AdjustmentRepository extends JpaRepository<Adjustment, Long> {

   public AdjustmentInterface findByConsumerNoAndPostingBillMonth(String consumerNo , String postingBillMonth);

   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeleted(String consumerNo , boolean posted , boolean deleted);

   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo , boolean posted , boolean deleted,String approvalStatus);

   public List<AdjustmentInterface> findByConsumerNoAndApprovalStatus(String consumerNo ,String approvalStatus);

   public List<AdjustmentInterface> findByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted , boolean deleted);

   public AdjustmentInterface save(AdjustmentInterface adjustmentInterface);

   public List<AdjustmentInterface> findByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted);

   public List<AdjustmentInterface> findByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo, String postingBillMonth, boolean posted, boolean deleted);

   //count Methods returns count coded by nitish
   public long countByConsumerNo(String consumerNo);

   public long countByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted);

   public long countByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo, boolean posted, boolean deleted, String approvalStatus);

   public long countByConsumerNoAndApprovalStatus(String consumerNo,String approvalStatus);

   public long countByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted);

   //Pageable Method returns list on the basis of pageable properties coded by nitish
   public List<AdjustmentInterface> findByConsumerNo(String consumerNo, Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByConsumerNoOrderByPostingBillMonthAscending(@Param("consumerNo") String consumerNo, Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByConsumerNoOrderByPostingBillMonthDescending(@Param("consumerNo") String consumerNo, Pageable pageable);

   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeleted(String consumerNo , boolean posted , boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthAscending(@Param("consumerNo") String consumerNo , @Param("posted") boolean posted , @Param("deleted") boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthDescending(@Param("consumerNo") String consumerNo , @Param("posted") boolean posted , @Param("deleted") boolean deleted,Pageable pageable);

   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo , boolean posted , boolean deleted,String approvalStatus,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.posted= :posted and a.deleted= :deleted and a.approvalStatus= :approvalStatus order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthAscending(@Param("consumerNo")String consumerNo ,@Param("posted") boolean posted,@Param("deleted") boolean deleted,@Param("approvalStatus") String approvalStatus,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.posted= :posted and a.deleted= :deleted and a.approvalStatus= :approvalStatus order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthDescending(@Param("consumerNo")String consumerNo ,@Param("posted") boolean posted,@Param("deleted") boolean deleted,@Param("approvalStatus") String approvalStatus,Pageable pageable);

   public List<AdjustmentInterface> findByConsumerNoAndApprovalStatus(String consumerNo ,String approvalStatus,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.approvalStatus= :approvalStatus order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending(@Param("consumerNo")String consumerNo ,@Param("approvalStatus")String approvalStatus,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.approvalStatus= :approvalStatus order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending(@Param("consumerNo")String consumerNo ,@Param("approvalStatus")String approvalStatus,Pageable pageable);

   public List<AdjustmentInterface> findByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.locationCode= :locationCode and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(@Param("locationCode")String locationCode,@Param("posted") boolean posted,@Param("deleted") boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.locationCode= :locationCode and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(@Param("locationCode")String locationCode,@Param("posted") boolean posted,@Param("deleted") boolean deleted,Pageable pageable);

   public List<AdjustmentInterface> findByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.code= :code and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
   public List<AdjustmentInterface> findByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(@Param("consumerNo")String consumerNo, @Param("code")int code, @Param("posted")boolean posted, @Param("deleted")boolean deleted,Pageable pageable);

   @Query("from #{#entityName} a where a.consumerNo= :consumerNo and a.code= :code and a.posted= :posted and a.deleted= :deleted order by to_date(a.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
   public List<AdjustmentInterface> findByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(@Param("consumerNo")String consumerNo, @Param("code")int code, @Param("posted")boolean posted, @Param("deleted")boolean deleted,Pageable pageable);

   public List<AdjustmentInterface> findByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo, String postingBillMonth, boolean posted, boolean deleted,Pageable pageable);
}
