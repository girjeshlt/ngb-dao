package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface AgricultureBill6MonthlyDAOInterface<T> extends DAOInterface<T>{

    public T getTopByConsumerNo(String consumerNo);
    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);
}
