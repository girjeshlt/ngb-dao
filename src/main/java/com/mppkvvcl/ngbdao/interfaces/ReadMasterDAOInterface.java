package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface ReadMasterDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByConsumerNo(String ConsumerNo);

    public T getTopByConsumerNoOrderByReadingDateDesc(String ConsumerNo);

    public T getTopByConsumerNoOrderByIdDesc(String ConsumerNo);

    public List<T> getByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag);

    public List<T> getByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth);

    public T getByConsumerNoAndReplacementFlag(String consumerNo, String replacementFlag);

    public List<T> getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag, Boolean usedOnBill);

    public List<T> getTop2ByConsumerNoAndBillMonthOrderByIdDesc(String consumerNo, String billMonth);

    public List<T> getByConsumerNoOrderByBillMonthDescendingWithPageable(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByBillMonthAscendingWithPageable(String consumerNo, Pageable pageable);

    public long getCountByConsumerNo(String consumerNo);

    public List<T> getByConsumerNoAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String replacementFlag, Boolean usedOnBill);
}
