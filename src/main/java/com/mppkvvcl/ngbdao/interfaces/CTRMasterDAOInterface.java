package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface CTRMasterDAOInterface<T> extends DAOInterface<T>{
    public List<T> getBySerialNo(String serialNo);
    public List<T> getByMake(String make);
    public T getByIdentifier(String identifier);
    public T getBySerialNoAndMake(String serialNo,String make);
}
