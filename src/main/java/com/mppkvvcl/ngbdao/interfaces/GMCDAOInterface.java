package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface GMCDAOInterface<T> extends DAOInterface<T>{
    public T getBySubcategoryCodeAndTariffId(long subcategoryCode,long tariffId);
}
