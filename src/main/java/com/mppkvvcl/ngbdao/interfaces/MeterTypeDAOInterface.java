package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface MeterTypeDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByMeterPhase(String meterPhase);
    public List<? extends T> getAll();
}
