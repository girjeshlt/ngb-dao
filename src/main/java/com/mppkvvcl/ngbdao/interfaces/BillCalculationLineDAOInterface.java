package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by PREETESH on 12/12/2017.
 */
public interface BillCalculationLineDAOInterface <T> extends DAOInterface<T>{

    public List<T> getByBillId(long billId);

    public List<T> addAll(List<T> list);
}