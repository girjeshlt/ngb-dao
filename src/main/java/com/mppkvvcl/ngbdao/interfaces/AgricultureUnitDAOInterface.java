package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface AgricultureUnitDAOInterface<T> extends DAOInterface<T> {

    public T getBySubcategoryCodeAndBillMonth(long subcategoryCode,String billMonth);
}
