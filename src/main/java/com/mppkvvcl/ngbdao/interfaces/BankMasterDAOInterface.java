package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface BankMasterDAOInterface<T> extends DAOInterface<T>  {
    List<? extends T> getAll();
}
