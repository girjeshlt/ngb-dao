package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

public interface SubsidyDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByCategoryAndIsBplAndSubcategoryCodeAndDate(String category, boolean isBpl,long subcategoryCode,Date date);

}
