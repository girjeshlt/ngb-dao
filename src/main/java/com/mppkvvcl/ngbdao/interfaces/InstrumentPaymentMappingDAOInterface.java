package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface InstrumentPaymentMappingDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByInstrumentDetailIdOrderByPaymentIdAsc(long instrumentDetailId);

    public T getByPaymentId(long id) ;

    public void delete(T instrumentPaymentMappingInterface);

}
