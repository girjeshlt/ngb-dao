package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface FCADAOInterface<T> extends DAOInterface<T>{

    public T getByDate(Date date);

    public List<T> getByEffectiveStartDateAndEffectiveEndDate(Date startDate,Date endDate);
}
