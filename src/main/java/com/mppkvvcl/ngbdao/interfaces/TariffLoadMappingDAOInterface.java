package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

public interface TariffLoadMappingDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByTariffDetailId(long tariffDetailId);

    public List<T> getByLoadDetailId(long loadDetailId);

    public List<T> getByTariffDetailIdAndLoadDetailId(long tariffDetailId,long loadDetailId);

    public List<T> getByTariffDetailIdAndLoadDetailIdAndIsTariffChangeAndIsLoadChange(long tariffDetailId,long loadDetailId,boolean isTariffChange,boolean isLoadChange);

    public List<T> getByTariffDetailIdAndLoadDetailIdAndIsTariffChange(long tariffDetailId,long loadDetailId,boolean isTariffChange);

    public T getTopByTariffDetailIdOrderByIdDesc(long tariffDetailId);

    public T getTopByLoadDetailIdOrderByIdDesc(long loadDetailId);

}
