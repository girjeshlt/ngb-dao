package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

public interface ExcessDemandSubcategoryMappingDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);
}
