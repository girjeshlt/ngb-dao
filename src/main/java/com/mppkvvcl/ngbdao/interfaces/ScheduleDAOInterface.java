package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface ScheduleDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByGroupNoOrderByIdDesc(String groupNo);

    public List<T> getByBillMonth(String billMonth);

    public List<T> getByBillStatus(String billStatus);

    public List<T> getByBillMonthAndBillStatus(String billMonth,String billStatus);

    public List<T> getByBillStatusAndSubmitted(String billStatus,String submitted);

    public List<T> getByR15Status(String r15Status);

    public T getByGroupNoAndBillMonthAndBillStatus(String groupNo, String billMonth, String billStatus);

    public List<T> getByBillMonthAndBillStatusAndSubmitted(String billMonth, String billStatus, String submitted);

    public List<T> getByBillMonthAndR15Status(String billMonth,String r15Status);

    public List<T> getByGroupNoAndBillStatusAndSubmitted(String groupNo, String billStatus, String submitted);

    public T getTopByGroupNoOrderByIdDesc(String groupNo);

    public T getTopByGroupNoAndBillStatusOrderByIdDesc(String groupNo,String billStatus);

    public T getByGroupNoAndBillMonth(String groupNo, String lastBillMonth);

    public long getCountByGroupNo(String groupNo);

    public List<T> getTop2ByGroupNoOrderByIdDesc(String groupNo);

    public T getByGroupNoAndBillMonthAndBillStatusAndSubmitted(String groupNo, String previousBillMonth, String billStatus, String submittedStatus);

    //pageable methods
    public List<T> getByGroupNo(String groupNo, Pageable pageable);

    public List<T> getByGroupNoOrderByBillMonthDescending(String groupNo, Pageable pageable);

    public List<T> getByGroupNoOrderByBillMonthAscending(String groupNo, Pageable pageable);
}
