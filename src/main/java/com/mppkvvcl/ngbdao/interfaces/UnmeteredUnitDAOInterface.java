package com.mppkvvcl.ngbdao.interfaces;

public interface UnmeteredUnitDAOInterface<T> extends DAOInterface<T>{
    public T getByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode);
}
