package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface NSCStagingDAOInterface<T> extends DAOInterface<T> {

    public boolean existsByLocationCodeAndConnectionDateBetween(String locationCode, Date fromDate, Date toDate);
}
