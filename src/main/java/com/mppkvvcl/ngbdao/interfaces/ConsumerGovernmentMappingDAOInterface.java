package com.mppkvvcl.ngbdao.interfaces;

/**
 * Created by SHIVANSHU on 15-09-2017.
 */
public interface ConsumerGovernmentMappingDAOInterface<T> extends DAOInterface<T> {

    public T getByConsumerNo(String consumerNo);
}
