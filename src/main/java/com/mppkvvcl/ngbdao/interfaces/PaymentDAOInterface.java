package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface PaymentDAOInterface<T> extends DAOInterface<T> {

    public long getCountByConsumerNo(String consumerNo);

    public List<T> getByConsumerNo(String consumerNo);

    public List<T> getByConsumerNo(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByPostingBillMonthAscendingWithPageable(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByPostingBillMonthDescendingWithPageable(String consumerNo, Pageable pageable);

    public List<T> getByLocationCodeAndPostingBillMonth(String locationCode, String postingBillMonth);

    public List<T> getByConsumerNoAndDeletedAndPosted(String consumerNo, boolean deleted, boolean posted);

    public List<T> getByLocationCodeAndPayWindowAndPayDateAndDeleted(String locationCode, String payWindow, Date payDate, boolean deleted);

    public List<T> getByConsumerNoAndPostingBillMonthAndDeletedAndPosted(String consumerNo, String billMonth, boolean deleted, boolean posted);

    public List<T> getByConsumerNoAndPayModes(final String consumerNo, final String[] payModes);

    public List<T> getByConsumerNoAndDeleted(String consumerNo,boolean deleted);

    public List<T> add(List<T> list);
}
