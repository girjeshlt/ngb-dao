package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface EmployeeMasterDAOInterface<T> extends DAOInterface<T>{

    public T getByEmployeeNo(String employeeNo);

    public List<T> getByCompany(String company);

    public T getByEmployeeNoAndStatus(String employeeNo, String status);

    public T getByEmployeeNoAndTypeAndStatus(String employeeNo, String type, String status);
}
