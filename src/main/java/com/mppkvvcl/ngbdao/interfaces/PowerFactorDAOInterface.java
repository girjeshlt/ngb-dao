package com.mppkvvcl.ngbdao.interfaces;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface PowerFactorDAOInterface<T> extends DAOInterface<T> {

    public T getByPowerFactorValueAndDate(BigDecimal pfValue, Date date);

    public List<? extends T> getAll();

    public List<? extends T> getByDate(Date date);
}
