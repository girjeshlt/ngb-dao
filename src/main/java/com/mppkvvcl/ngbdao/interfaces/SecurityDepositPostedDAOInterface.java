package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface SecurityDepositPostedDAOInterface<T> extends DAOInterface<T>  {

    public List<T> getBySecurityDepositId(long securityDepositId );
    public List<T> getByBillMonth(String billMonth );
    public List<T> add(List<T> list);

}
