package com.mppkvvcl.ngbdao.interfaces;
/**
 * code by nitish on 23-09-2017
 * @param <T>
 */
public interface SECircleMappingDAOInterface<T> extends DAOInterface<T> {
    public T getByUsername(String username);

    public T getByCircleId(long circleId);
}
