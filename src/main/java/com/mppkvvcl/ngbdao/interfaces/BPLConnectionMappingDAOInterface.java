package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface BPLConnectionMappingDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByConsumerNo(String consumerNo);

    public List<T> getBetweenStartDateAndEndDate(Date givenDate);

    public List<T> getByBplNo(String bplNo);

    public List<T> getByStatus(String status);

    public List<T> getByConsumerNoAndStatus(String consumerNo,String status);

    public List<T> getByBplNoAndStatus(String bplNo,String status);
}
