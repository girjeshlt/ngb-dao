package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface InstrumentDishonourDAOInterface<T> extends DAOInterface<T>{

    T getTopByConsumerNoOrderByIdDesc(String consumerNo);

}
