package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.BillInterface;

import java.util.List;

public interface BillCorrectionDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByConsumerNoAndBillMonthAndDeleted(String consumerNo ,String billMonth, boolean deleted);
}
