package com.mppkvvcl.ngbdao.interfaces;

public interface IrrigationSchemeDAOInterface<T> extends DAOInterface<T> {
    public T getByConsumerNo(String consumerNo);
}
