package com.mppkvvcl.ngbdao.interfaces;

/**
 * Created by PREETESH on 11/7/2017.
 */
public interface GMCAccountingDAOInterface<T> extends DAOInterface<T>{
    public T getByConsumerNo(String consumerNo);

    public T getByConsumerNoAndCurrentBillMonth(String consumerNo, String currentBillMonth);
}