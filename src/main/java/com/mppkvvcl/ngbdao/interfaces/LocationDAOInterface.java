package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface LocationDAOInterface<T> extends  DAOInterface<T>{

    public T getByLocationCode(String locationCode);

    public T getByZoneId(Long zoneId);

    public List<T> getByDivisionId(Long zoneId);

    public List<T> getByCircleId(Long circleId);

    public List<T> getByRegionId(Long regionId);

    public List<T> getByDiscomId(Long discomId);
}
