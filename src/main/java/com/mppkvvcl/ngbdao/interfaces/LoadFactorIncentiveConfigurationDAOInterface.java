package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 12/23/2017.
 */
public interface LoadFactorIncentiveConfigurationDAOInterface<T> extends DAOInterface<T> {
    public T getByLoadFactorValueAndDate(BigDecimal lfValue,Date date);
    public List<T> getByDate(Date date);

}
