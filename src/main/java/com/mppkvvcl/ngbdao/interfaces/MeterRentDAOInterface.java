package com.mppkvvcl.ngbdao.interfaces;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface MeterRentDAOInterface<T> extends DAOInterface<T> {

    public T getByMeterCodeAndMeterCapacity(String meterCode, String meterCapacity);
    public List<T> getByMeterCode(String meterCode);
    public List<T> getByMeterRent(BigDecimal meterRent);
    public T getByMeterCodeAndEffectiveStartDate(String meterCode,Date effectiveStartDate);
    public T getByMeterRentAndEffectiveStartDate(BigDecimal meterRent, Date effectiveStartDate);

}
